# --- First database schema
 
# --- !Ups

CREATE TABLE params (
  id                        SERIAL PRIMARY KEY,
  name                      VARCHAR(190) NOT NULL UNIQUE,
  value                     VARCHAR(190) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE permissions (
  id                        SERIAL PRIMARY KEY,
  name                      VARCHAR(100) NOT NULL UNIQUE
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE users (
  id                        SERIAL PRIMARY KEY,
  email                     VARCHAR(100) NOT NULL UNIQUE,
  hash                      VARCHAR(100),
  permission_id             BIGINT UNSIGNED NOT NULL,
  avatar_id                 BIGINT,
  user_status               VARCHAR(100) NOT NULL,
  account_status            VARCHAR(100) NOT NULL,
  name                      VARCHAR(100),
  surname                   VARCHAR(100),
  timezone                  VARCHAR(100) NOT NULL,
  registered                DATETIME NOT NULL,
  phone                     VARCHAR(100),
  user_type                 VARCHAR(100) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE content (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED NOT NULL,
  parent_id                 BIGINT UNSIGNED,
  title                     VARCHAR(190),
  data                      LONGTEXT,
  status                    VARCHAR(100),
  kind                      VARCHAR(100) NOT NULL,
  role                      VARCHAR(100) NOT NULL,
  mime_type                 VARCHAR(100),
  created                   DATETIME NOT NULL,
  modified                  DATETIME
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE friends (
  id                        SERIAL PRIMARY KEY,
  initiator_id              BIGINT UNSIGNED NOT NULL,
  friend_id                 BIGINT UNSIGNED NOT NULL,
  status                    VARCHAR(100),
  requested                 DATETIME NOT NULL,
  created                   DATETIME
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE instagram_tokens (
  id                        SERIAL PRIMARY KEY,
  user_id                   BIGINT UNSIGNED NOT NULL,
  token_id                  INT UNSIGNED NOT NULL,
  code                      VARCHAR(100),
  token                     VARCHAR(100),
  secret                    VARCHAR(100),
  raw                       TEXT,
  UNIQUE (user_id, token_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE activation_codes (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED NOT NULL,
  action                    INT UNSIGNED NOT NULL,
  start                     BIGINT UNSIGNED,
  period                    BIGINT UNSIGNED,
  code                      VARCHAR(100)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE robot_instances (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED NOT NULL,
  robot_name                VARCHAR(190) NOT NULL,
  status                    VARCHAR(190) NOT NULL,
  status_descr              TEXT,
  serialized_id             BIGINT UNSIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE robot_states (
  id                        SERIAL PRIMARY KEY,
  instance_id               BIGINT UNSIGNED NOT NULL,
  data                      TEXT NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE followed_before (
  user_id                   BIGINT UNSIGNED NOT NULL,
  account_id                BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (user_id, account_id) 
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE subscriber_before (
  user_id                   BIGINT UNSIGNED NOT NULL,
  account_id                BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (user_id, account_id) 
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;


# --- !Downs

DROP TABLE IF EXISTS friends;

DROP TABLE IF EXISTS content;
 
DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS permissions;

DROP TABLE IF EXISTS params;

DROP TABLE IF EXISTS instagram_tokens;

DROP TABLE IF EXISTS activation_codes;

DROP TABLE IF EXISTS robot_instances;

DROP TABLE IF EXISTS robot_states;

DROP TABLE IF EXISTS instagram_users;

DROP TABLE IF EXISTS follower_before;

DROP TABLE IF EXISTS subscriber_before;

