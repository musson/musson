application.name = Муссон

menu = Меню
go_to_content = Перейти к содержимому


module.main.name = Главная

admin.user.profile=Профиль
admin.user.logout=Выйти

permissions.admin=Администратор
permissions.normal_user=Пользователь
permissions.guest_user=Гость

module.users.create.success = Пользователь {0} успешно создан
module.users.create = Создать
module.users.name = Пользователи
module.users.user = Пользователь
module.users.profile = Профиль пользователя
module.users.user.created = Создан
module.users.user.user_status = Статус пользователя
module.users.user.user_status.online = Онлайн
module.users.user.user_status.offline = Офлайн
module.users.user.account_status = Статус аккаунта
module.users.user.account_status.enabled = Активный
module.users.user.account_status.disabled = Неактивный
module.users.user.account_status.wait_for_activation = Ожидает активации
module.users.user.status = Статус
module.users.user.email = Электронная почта
module.users.list = Список пользователей
module.users.page.uploadpage.error = Не могу загрузить страницу
module.users.remove.success = Пользователь {0} был успешно удален.
module.users.user.netstatus.online = Онлайн
module.users.user.profile.member_since=Зарегистрирован:
module.users.user.pages=Страницы
module.users.user.mediafiles=Медиа 
module.users.user.user_type=Тип пользователя
module.users.user.user_type.subscriber=Подписчик
module.users.user.user_type.normal=Обычный
module.users.user.user_type.client=Клиент


module.posts.name = Записи
module.posts.post.title = Заголовок
module.posts.post.author = Автор
module.posts.post.data = Дата
module.posts.post.content = Содержимое
module.posts.edit = Редактировать
module.posts.tabs.tab_all_posts = Все записи
module.posts.tabs.tab_published_posts = Опубликованные
module.posts.addnew = Добавить новую
module.posts.addpost = Добавить запись
module.posts.saveAsDraft = Сохранить как черновик
module.posts.publish = Опубликовать
module.posts.update = Обновить
module.posts.new.success = Запись {0} была успешно создана.
module.posts.list = Список записей
module.posts.post.status = Статус
module.posts.post.status.draft = Черновик
module.posts.post.status.published = Опубликовано
module.posts.post.status.error = Неверный статус
module.posts.update.success = Запись {0} была успешно обновлена.
module.posts.remove.success = Запись {0} была успешно удалена.
module.posts.post.uploadpost.error = Не могу загрузить запись


module.pages.name = Страницы
module.pages.page.title = Заголовок
module.pages.page.author = Автор
module.pages.page.data = Дата
module.pages.page.content = Содержимое
module.pages.edit = Редактировать
module.pages.tabs.tab_all_pages = Все страницы
module.pages.tabs.tab_published_pages = Опубликованные
module.pages.addnew = Добавить новую
module.pages.addpage = Добавить страницу
module.pages.saveAsDraft = Сохранить как черновик
module.pages.publish = Опубликовать
module.pages.update = Обновить
module.pages.new.success = Страница {0} была успешно создана.
module.pages.list = Список страниц
module.pages.page.status = Статус
module.pages.page.status.draft = Черновик
module.pages.page.status.published = Опубликовано
module.pages.page.status.error = Неверный статус
module.pages.update.success = Страница {0} была успешно обновлена.
module.pages.remove.success = Страница {0} была успешно удалена.
module.pages.page.uploadpage.error = Не могу загрузить страницу


module.media.name = Медиафайлы
module.media.file.title = Файл
module.media.file.author = Автор
module.media.file.owner = Владелец
module.media.file.data = Дата
module.media.file.remove.error = Не удалось удалить файл
module.media.list = Список медиафайлов
module.media.remove.success = Медифайл {0} был успешно удален.
module.media.page.uploadpage.error = Не могу загрузить страницу
module.media.addnew = Добавить новые
module.media.addmedia = Добавить медиафайлы


module.settings.name = Настройки
module.settings.base = Основные
module.settings.media = Медиафайлы
module.settings.base.update.success = Базовые настройки успешно изменены
module.settings.base.project_name = Название сайта
module.settings.base.project_descr = Описание сайта
module.settings.base.timezone = Часовой пояс
module.settings.base.project_descr.descr = Объясните в нескольких словах, о чём этот сайт.
module.settings.base.timezone.descr = Выберите часовой пояс, в котором вы хотите отображать время на сайте
module.settings.base.save_changes = Сохранить изменения

module.instagram.name = Инстаграм
module.instagram.users = Пользвователи
module.instagram.user.bots = Мои боты  
module.instagram.bots = Боты
module.instagram.users.user = Пользователь
module.instagram.users.user.module_active = Активный модуль Instagram  
module.instagram.users.user.all_robots = Всего роботов
module.instagram.users.user.active_robots = Акитвных роботов 
module.instagram.user.module.status_yes = Да
module.instagram.user.module.status_no = Нет
module.instagram.user.robots = Роботы
module.instagram.bots.bot.name = Имя робота
module.instagram.bots.bot.id = Номер
module.instagram.bots.bot.descr = Описание
module.instagram.bots.bot.status = Состояние
module.instagram.bots.status.paused = Пауза
module.instagram.bots.status.processed = Работает
module.instagram.bots.status.stopped = Остановлен
module.instagram.bots.status.ready = Готов к работе  
module.instagram.bots.status.unknown = Не известно  
module.bots.bot.config.submit = Создать
module.bots.bot.likerbot1.tag = Строка для поиска хэштэгов
module.bots.bot.new.success = Робот {0} успешно создан   
module.instagram.bots.status.instopprocess = Останавливается...
module.bots.bot.relationsbot1.tag = Строка для поиска хэштэгов
module.bots.bot.relationslikerbot1.tag = Строка для поиска хэштэгов
module.bots.bot.user_name = Имя пользователя
module.instagram.bots.bot.not_robots_created = Вы не создали ни одного робота.

module.instagram.bots.bot.statsBot1.mediacount = Медиа
module.instagram.bots.bot.statsBot1.subscribers = Ваши подписчики
module.instagram.bots.bot.statsBot1.followers = Ваши подписки

module.bots.bot.statsbot1.period = Период в миллисекундах (от одной минуты до одного дня)

validation.errors = Пожалуйста, исправьте ошибки в форме.

error.required=Это поле должно быть заполнено
error.email=Некорректная электронная почта
error.email.exists=Такой email уже зарегистрирован
constraint.required=Обязательное поле*

page.login=Авторизация

module.bots.bot.tagslikerbot1.tags = Хэш-тэги разделенные пробелом

form.register.name=Имя
form.register.surname=Фамилия

form.login.email=Электронаня почта
form.login.pass=Пароль
form.login.submit=Войти

form.register.email=Электронаня почта
form.register.pass=Пароль
form.register.submit=Отправить

module.instagram.bots.uploadpage.error=Не могу загрузить страницу

account.confirm.success=Ваш аккаунт успешно подтвержден!

register.afterRegister = Код для активации аккаунта отправлен вам на почту. Срок активации истекает через 24 часа.

login.page=Вход
register.page=Регистрация
register.page.title=Регистрация
login.page.title=Вход

module.instagram.bots.bot.success = Успешно
module.instagram.bots.bot.badRequest = Ошибка
module.instagram.bots.bot.before = Выполнено ранее
module.instagram.bots.bot.accessDenied = Доступ запрещен
module.instagram.bots.bot.unknown = Неизвестно

module.bots.bot.state.initial = Инициализация 
module.bots.bot.state.unknown = Неизвестно
module.bots.bot.state.finished = Завершено
module.bots.bot.state.finished_on_error = Завершено в результате ошибки

module.instagram.bots.bot.state.get_tag_forms = Получение слово-форм тэга
module.instagram.bots.bot.state.get_recent_posts_forms = Получаение недваних постов
module.instagram.bots.bot.state.like_posts = Лайкает посты
module.instagram.bots.bot.state.get_users = Получение пользователей
module.instagram.bots.bot.state.set_relations = Устанавливает связи
module.instagram.bots.bot.state.remove_users = Удаляет пользователей
module.instagram.bots.bot.state.collect_info = Собирает информацию

module.instagram.bots.bot.all = Всего
module.instagram.bots.bot.results = Результаты 

module.instagram.bots.bot.unknownResult = Неизвестный результат
module.instagram.bots.bot.currentResult = Текущий результат

module.instagram.bots.bot.localIndex = Номер в партии
module.instagram.bots.bot.userId = Номер пользвоателя
module.instagram.bots.bot.userName = Имя пользователя
module.instagram.bots.bot.status = Статус
module.instagram.bots.bot.hashTag = Хэш-тэг
module.instagram.bots.bot.mediaId = Номер поста

module.instagram.bots.module.not_authorized = Вы не авторизованы в Инстаграмм. 
module.settings.includes=Вложения
module.settings.includes.update.success=Настройки вложений успешно обновлены!
module.settings.includes.header=Заголовочные вложения
module.settings.includes.body=Вложения в тело
module.settings.includes.footer=Вложения в подвал
module.settings.includes.save_changes=Сохранить изменения
module.settings.base.loginIsVisible=Видна ли страница входа?
module.settings.base.registerIsActive=Активна ли регистрация нового пользователя?

module.instagram.bots.bot.statsBot1.velocityFollowers=Скорость изменения ваших подписчиков

