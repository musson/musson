name := """musson"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "com.typesafe.play" %% "play-mailer" % "2.4.0",
  "com.github.t3hnar" %% "scala-bcrypt" % "2.4",
  "mysql" % "mysql-connector-java" % "5.1.27",
  "jp.t2v" %% "play2-auth" % "0.12.0",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "bootstrap" % "3.2.0",
  "org.webjars" % "requirejs" % "2.1.11-1",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "jquery-easing" % "1.3-1",
  "org.webjars" % "jquery-waypoints" % "2.0.3",
  "org.webjars" % "FlexSlider" % "2.2.2",
  "org.webjars" % "font-awesome" % "4.1.0",
  "org.webjars" % "nanoScrollerJS" % "0.7.6",
  "org.webjars" % "ckeditor" % "4.4.1",
  "org.webjars" % "select2" % "3.5.0",
  "org.webjars" % "jquery-ui" % "1.11.0",
  "org.webjars" % "dropzone" % "3.7.1",
  "org.webjars" % "modernizr" % "2.7.1",
  "org.webjars" % "respond" % "1.4.2",
  "org.webjars" % "html5shiv" % "3.7.2",
  "org.webjars" % "retinajs" % "0.0.2"
)
