package controllers

import models.User
import models.Content
import play.api.mvc.Action
import play.api.mvc.Controller
import jp.t2v.lab.play2.auth.AuthElement
import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.Param
import jp.t2v.lab.play2.stackc.RequestWithAttributes

object Posts extends Controller with OptionalAuthElement with AuthConfigImpl {

  def show(id: Long) = StackAction { implicit request =>
    Content.findById(id).map { post =>
      Dynamic.render("postPage", post, request2flash, request2lang, loggedIn.getOrElse(User.guest)) match {
        case Some(page) => Ok(page)
        case None       => NotFound
      }
    }.getOrElse(NotFound)
  }

  def getLastPosts() =
    Content.findLastLimited(
      Param.findByName(
        Param.PARAM_LAST_POSTS_LIMIT).map(limitParam => limitParam.int) getOrElse (Param.PARAM_VALUE_DEFAULT_LAST_POSTS_LIMIT), "kind" -> Content.KIND_POST)

}
