package controllers

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar

import models.Content
import models.Param
import play.api.mvc.Controller

object Media extends Controller {

  val yearDayFolderDateFormat = new SimpleDateFormat("yyyy" + File.separator + "MM")

  def getYearDay = yearDayFolderDateFormat.format(Calendar.getInstance().getTime)

  def getFilesFolderParam = Param.findByName(Param.PARAM_FILES_FOLDER).map { param => param.value }.getOrElse(Param.PARAM_VALUE_DEFAULT_FILES_FOLDER)

  def getFilesFolderAbs = FSUtils.getAbsPathForPublicPath(getFilesFolderParam)

  def getFileRelPathForNow(file: String) = getYearDay + File.separator + file

  def getFileAbsPath(file: String) = getFilesFolderAbs + File.separator + file

  def getAttachmentLink(id: Long): String = Content.findById(id).map(FSUtils.CONTENT_FOLDER + File.separator + getFilesFolderParam + File.separator + _.data.getOrElse("")).getOrElse("")

  def getAttachmentLink(id: Option[Long]): String = id.map { lid => getAttachmentLink(lid) }.getOrElse("")

}