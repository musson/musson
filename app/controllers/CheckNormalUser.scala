package controllers

import jp.t2v.lab.play2.auth.AsyncAuth
import jp.t2v.lab.play2.auth.LoginLogout
import models.Permission
import play.Logger
import play.api.mvc.Controller
import play.api.mvc.Result
import controllers.AppImplicits.appContext
import play.api.mvc.Action
import play.api.mvc.AnyContent
import play.api.mvc.Request
import scala.concurrent.ExecutionContext

trait CheckNormalUser extends Controller with AsyncAuth with LoginLogout with AuthConfigImpl {

  def checkNormalUserAction(authority: Authority, uid: Long)(f: User => (Request[AnyContent] => Result))(implicit context: ExecutionContext): Action[(AnyContent, User)] = {
    authorizedAction(authority)({ implicit user =>
      implicit request =>
        user.permission.name match {
          case Permission.PERMISSION_ADMINISTRATOR => f.apply(user)(request)
          case _ => if (user.id == uid) {
            f.apply(user)(request)
          } else {
            Logger.debug("Access denied for user: " + user.email + " with id = " + user.id)
            Forbidden
          }
        }
    })
  }

}