package controllers

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.reflect.ClassTag
import scala.reflect.classTag

import jp.t2v.lab.play2.auth.AuthConfig
import models.Permission
import play.api.mvc.RequestHeader
import play.api.mvc.Results.Forbidden
import play.api.mvc.Results.Redirect

trait AuthConfigImpl extends AuthConfig {

  type Id = Long

  type User = models.User

  type Authority = Permission

  val idTag: ClassTag[Id] = classTag[Id]

  val sessionTimeoutInSeconds = 36000

  def resolveUser(id: Id)(implicit ctx: ExecutionContext) = Future successful (models.User.findById(id))

  def loginSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext) = {
    val uri = request.session.get(Application.RETURN_URL).getOrElse(routes.Admin.admin.toString())
    Future.successful(Redirect(uri).withSession(request.session - Application.RETURN_URL))
  }

  def logoutSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext) = Future.successful(Redirect(routes.Application.login))

  def authenticationFailed(request: RequestHeader)(implicit ctx: ExecutionContext) = Future.successful(Redirect(routes.Application.login).withSession(Application.RETURN_URL -> request.uri))

  def authorizationFailed(request: RequestHeader)(implicit ctx: ExecutionContext) = Future.successful(Forbidden("no permission"))

  def authorize(user: User, authority: Authority)(implicit ctx: ExecutionContext) = Future.successful((user.permission.name, authority.name) match {
    case (Permission.PERMISSION_ADMINISTRATOR, _) => true
    case (Permission.PERMISSION_NORMAL_USER, Permission.PERMISSION_NORMAL_USER) => true
    case _ => false
  })
  
}
