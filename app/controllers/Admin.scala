package controllers

import play.Logger
import controllers.AppImplicits.appContext
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import play.api.mvc.Controller

object Admin extends Controller with AsyncAuth with AuthConfigImpl {

  def admin = authorizedAction(Permission.normalUser) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => admin()")
      Redirect(controllers.modules.routes.Main.show)
  }

  def modules = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => modules()")
      Redirect(controllers.modules.routes.Main.show)
  }

}
