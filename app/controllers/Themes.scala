package controllers

import models.Param
import play.api.mvc.Controller

object Themes extends Controller {

  def getThemeName = Param.findByName(Param.PARAM_THEME_NAME).map(_.value).getOrElse(Param.PARAM_VALUE_DEFAULT_THEME_NAME)

}
