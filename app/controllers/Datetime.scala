package controllers

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.TimeZone
import scala.Array.canBuildFrom
import scala.collection.mutable.Map
import java.util.Date

trait DatetimeTrait {

  val childs = Map[String, DatetimeTrait]()

}

case class DatetimeNode() extends DatetimeTrait

case class EndDatetimeNode(id: String) extends DatetimeTrait

object Datetime extends DatetimeTrait {

  val dbDateTimeFormat = "yyyy:MM:dd HH:mm:ss"

  def defaultFormatter = new SimpleDateFormat(dbDateTimeFormat)

  def addTimezoneId(datetime: DatetimeTrait, unperfromedID: String, curID: String) {
    curID.indexOf("/") match {
      case -1 => datetime.childs.getOrElseUpdate(curID, EndDatetimeNode(unperfromedID))
      case index => {
        val nextCurID = curID.substring(0, index: Int)
        addTimezoneId(datetime.childs.getOrElseUpdate(nextCurID, DatetimeNode()), unperfromedID, curID.substring(index + 1))
      }
    }
  }

  {
    TimeZone.getAvailableIDs() foreach (id => addTimezoneId(Datetime, id, id))
  }

  def timezone = models.Param.findByName(models.Param.PARAM_TIMEZONE).map { param => param.value }.getOrElse(models.Param.PARAM_VALUE_DEFAULT_TIMEZONE)

  def formatter(zone: String) = {
    val formatter = new SimpleDateFormat(dbDateTimeFormat)
    formatter.setTimeZone(TimeZone.getTimeZone(zone))
    formatter
  }

  def dtsCurrentUTC = formatter("UTC").format(System.currentTimeMillis())

  def dtCurrentUTC = defaultFormatter.parse(dtsCurrentUTC)

  def dtsFromUTCDate(zone: String, date: Date) = formatter(zone).format(formatter("UTC").parse(defaultFormatter.format(date)))

  def dtsFromUTCDate(datetime: Date): String = dtsFromUTCDate(timezone, datetime)
}
