package controllers

import java.lang.reflect.Method

import play.Logger
import play.api.Play
import play.twirl.api.Html

object Dynamic {

  def render(page: String, args: Any*): Option[Html] = {
    renderDynamic("views.html.themes." + Themes.getThemeName + "." + page, args: _*)
  }

  def renderDynamic(viewClazz: String, args: Any*): Option[Html] = {
    try {
      val clazz: Class[_] = Play.current.classloader.loadClass(viewClazz)
      if (clazz == null)
        return None
      val render: Method = clazz.getDeclaredMethod("render", (for (arg <- args) yield arg.getClass): _*)
      val view = render.invoke(clazz, (for (arg <- args) yield arg.asInstanceOf[Object]): _*).asInstanceOf[Html]
      return Some(view)
    } catch {
      case ex: ClassNotFoundException => Logger.error("Html.renderDynamic() : could not find view " + viewClazz, ex)
    }
    return None
  }

}

