package controllers

import java.io.File
import java.nio.file.Paths

object FSUtils {

  val CONTENT_FOLDER = "content"

  def removeFileFromPublic(pathToFileRelPublic: String) = (new File(getAbsPathForPublicPath(pathToFileRelPublic))).delete()

  def getAbsPathForPublicPath(pathToFileRelPublic: String) = {
    Paths.get(Application.getClass().getResource("/").toURI()).getParent().getParent().getParent().toString() +
      File.separator + "target" + File.separator + "web" + File.separator +
      "public" + File.separator + "main" + File.separator + CONTENT_FOLDER + File.separator + pathToFileRelPublic;
  }

}
