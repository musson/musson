package controllers.modules.keno_landing

import controllers.AuthConfigImpl

import controllers.Datetime
import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.Permission
import models.User
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.ignored
import play.api.data.Forms.mapping
import play.api.data.Forms.optional
import play.api.data.Forms.text
import play.api.mvc.Controller
import play.api.mvc.Flash
import views.html
import play.api.i18n.Messages

case class ClientInfo(val name: Option[String], val email: String, val phone: Option[String]) {
  def user = User(
    email,
    None,
    Permission.normalUser,
    None,
    User.USER_STATUS_OFFLINE,
    User.ACCOUNT_STATUS_DISABLED,
    name,
    None,
    "UTC",
    phone, User.USER_TYPE_SUBSCRIBER)
}

case class SubscriberInfo(val email: String) {
  def user = User(
    email,
    None,
    Permission.normalUser,
    None,
    User.USER_STATUS_OFFLINE,
    User.ACCOUNT_STATUS_DISABLED,
    None,
    None,
    "UTC",
    None, User.USER_TYPE_CLIENT)
}

object KenoLanding extends Controller with OptionalAuthElement with AuthConfigImpl {

  private val clientInfoForm: Form[ClientInfo] = Form(
    mapping(
      "name" -> optional(text),
      "email" -> email.verifying(Messages("error.email.exists"), { User.findByEmail(_) == None }),
      "phone" -> optional(text))(ClientInfo.apply)(ClientInfo.unapply))

  private val subscriberInfoForm: Form[SubscriberInfo] = Form(
    mapping(
      "email" -> email.verifying(Messages("error.email.exists"), { User.findByEmail(_) == None }))(SubscriberInfo.apply)(SubscriberInfo.unapply))

  def landing = StackAction { implicit request =>
    implicit val user: User = loggedIn.getOrElse(User.guest)
    Ok(html.themes.keno_landing.landingPage(clientInfoForm, subscriberInfoForm))
  }

  def clientLanding = StackAction { implicit request =>
    val form = if (request2flash.get("error").isDefined) clientInfoForm.bind(request2flash.data) else clientInfoForm
    implicit val user: User = loggedIn.getOrElse(User.guest)
    Ok(html.themes.keno_landing.landingPage(form, subscriberInfoForm))
  }

  def submitClient = StackAction {
    implicit request =>
      val newForm = clientInfoForm.bindFromRequest
      newForm.fold(hasErrors = { form =>
        Redirect(controllers.modules.keno_landing.routes.KenoLanding.clientLanding).
          flashing(Flash(form.data) +
            ("error" -> Messages("validation.errors")))
      }, success = { newClientInfo =>
        User.create(newClientInfo.user).map { createdUser =>
          implicit val user: User = loggedIn.getOrElse(User.guest)
          Ok(html.themes.keno_landing.successClientAdded(createdUser)).
            flashing("success" -> Messages("module.keno_landing.new.client.success", "\"" + newClientInfo.email + "\""))
        }.getOrElse(BadRequest)
      })
  }

  def subscriberLanding = StackAction { implicit request =>
    val form = if (request2flash.get("error").isDefined) subscriberInfoForm.bind(request2flash.data) else subscriberInfoForm
    implicit val user: User = loggedIn.getOrElse(User.guest)
    Ok(html.themes.keno_landing.landingPage(clientInfoForm, form))
  }

  def submitSubscriber = StackAction {
    implicit request =>
      val newForm = subscriberInfoForm.bindFromRequest
      newForm.fold(hasErrors = { form =>
        Redirect(controllers.modules.keno_landing.routes.KenoLanding.subscriberLanding).
          flashing(Flash(form.data) +
            ("error" -> Messages("validation.errors")))
      }, success = { newSubscriberInfo =>
        User.create(newSubscriberInfo.user).map { createdUser =>
          implicit val user: User = loggedIn.getOrElse(User.guest)
          Ok(html.themes.keno_landing.successSubscriberAdded(createdUser)).
            flashing("success" -> Messages("module.keno_landing.new.subscriber.success", "\"" + newSubscriberInfo.email + "\""))
        }.getOrElse(BadRequest)
      })
  }

}
