package controllers.modules

import java.io.File
import controllers.AppImplicits.appContext
import scala.concurrent.Future
import controllers.AuthConfigImpl
import controllers.FSUtils
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Content
import models.Permission
import play.api.i18n.Messages
import play.api.mvc.Controller
import views.html
import controllers.Application

object Media extends Controller with AsyncAuth with AuthConfigImpl {

  def show = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Redirect(controllers.modules.routes.Media.list)
  }

  def list = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.media.list())
  }

  def page(pageNumber: Int, filter: String) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.media.filteredPage(Content.page(pageNumber, Application.assemblyFilter(filter, "kind" -> Content.KIND_ATTACHMENT): _*)))
  }

  def remove(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Content.delete(id) match {
        case 1L => {
          FSUtils.removeFileFromPublic(controllers.Media.getAttachmentLink(id))
          Redirect(controllers.modules.routes.Media.list).
            flashing("success" -> Messages("module.media.remove.success", "\"" + id + "\""))
        }
        case _ => BadRequest
      }
  }

  def create = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.media.create())
  }

  def upload = authorizedAction.async(parse.multipartFormData, Permission.administartor) { implicit user =>
    implicit request =>
      request.body.file("file").map { file =>
        val prefRelFilePath = controllers.Media.getFileRelPathForNow(file.filename)
        var relFilePath = prefRelFilePath
        var targetFile = new File(controllers.Media.getFileAbsPath(relFilePath))
        if (targetFile.exists()) {
          var counter = 1
          while (targetFile.exists()) {
            val startExtIndex = prefRelFilePath.indexOf(".")
            if (startExtIndex == 0) {
              relFilePath = "_" + counter + prefRelFilePath
            } else if (startExtIndex > 0) {
              relFilePath = prefRelFilePath.substring(0, startExtIndex) + "_" + counter + prefRelFilePath.substring(startExtIndex)
            } else {
              relFilePath = prefRelFilePath + "_" + counter
            }
            targetFile = new File(controllers.Media.getFileAbsPath(relFilePath))
            counter = counter + 1
          }
        }
        file.ref.moveTo(targetFile, true)
        Future.successful(
          Content.create(Content(user.id, Some(file.filename), Some(relFilePath), None, Content.KIND_ATTACHMENT, Content.ROLE_NO, file.contentType)).map {
            content => Ok(content.id.toString)
          }.getOrElse(BadRequest))
      }.getOrElse(Future.successful(BadRequest))
  }

}
