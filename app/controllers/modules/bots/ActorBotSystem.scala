package controllers.modules.bots

import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.collection.JavaConversions.enumerationAsScalaIterator
import scala.collection.JavaConversions.mapAsScalaConcurrentMap
import controllers.AppImplicits.appContext
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import controllers.Application
import play.Logger

/**
 *
 * BotProcessor executor as Actor. Used to controlling BotProcessor running.
 * All user interaction must be invoking by using BotActo API.
 *
 */
class BotActor(processor: BotProcessor) extends Actor {

  val isProcessed = new SyncedValue(false)

  val isStopped = new SyncedValue(false)

  val isRunned = new SyncedValue(false)

  val isInternalWorks = new SyncedValue(false)

  val isInStopProcess = new SyncedValue(false)

  var future: Future[_] = null

  def logDebug(message: String) = {
    Logger.debug("Processor " + processor.id + ": " + message)
  }

  def stopProcess = if (!isStopped.get && !isInStopProcess.get) {
    isRunned.set(false)
    isInStopProcess.set(true)
    if (future == null) {
      isStopped.set(true)
      isInStopProcess.set(false)
    }
  }

  def runActionInPausedProcess(f: => Unit) {
    if (isStopped.get) {
      while (isInternalWorks.get) Thread.sleep(500)
      sender ! ResultCurrent(processor.immutableResult)
    } else {
      if (isRunned.get) isProcessed.set(false)
      while (isInternalWorks.get) Thread.sleep(500)
      sender ! ResultCurrent(processor.immutableResult)
      if (isRunned.get) isProcessed.set(true)
    }
  }

  def receive = {
    case Start => {
      logDebug("Start message in progress...")
      if (!isStopped.get) {
        isRunned.set(true)
        isProcessed.set(true)
        future = Future {
          while (isRunned.get && !processor.isFinished) {
            while (!isProcessed.get && isRunned.get) Thread.sleep(500)
            if (isRunned.get) {
              isInternalWorks.set(true)
              processor.tick
              isInternalWorks.set(false)
              processor.pause
            }
          }
        }
        future onComplete {
          case Success(result) => {
            isStopped.set(true)
            isInStopProcess.set(false)
          }
          case Failure(error) => {
            isStopped.set(true)
            isInStopProcess.set(false)
          }
        }
      }
      logDebug("Start message successfully prepared")
    }
    case Stop => {
      logDebug("Stop message in progress...")
      stopProcess
      logDebug("Stop message successfully prepared")
    }
    case Pause => {
      logDebug("Pause message in progress...")
      if (!isStopped.get && isRunned.get) isProcessed.set(false)
      logDebug("Pause message successfully prepared")
    }
    case Continue => {
      logDebug("Continue message in progress...")
      if (!isStopped.get && isRunned.get) isProcessed.set(true)
      logDebug("Continue message successfully prepared")
    }
    case Remove => {
      logDebug("Remove message in progress...")
      stopProcess
      context.stop(self)
      logDebug("Remove message successfully prepared")
    }
    case GetStatus => {
      logDebug("GetStatus message in progress...")
      (isStopped.get, isRunned.get, isProcessed.get, isInStopProcess.get) match {
        case (_, _, _, true)         => sender ! StatusInStopProcess
        case (false, false, _, _)    => sender ! StatusReady
        case (false, true, false, _) => sender ! StatusPaused
        case (false, true, true, _)  => sender ! StatusProcessed
        case (true, _, _, _)         => sender ! StatusStopped
      }
      logDebug("GetStatus message successfully prepared")
    }
    case GetCurrentResult => {
      logDebug("GetCurrentResult message in progress...")
      runActionInPausedProcess { sender ! ResultCurrent(processor.immutableResult) }
      logDebug("GetCurrentResult message successfully prepared")
    }
    case SetConfig(config) => {
      logDebug("SetConfig message in progress...")
      runActionInPausedProcess { processor.configure(config) }
      logDebug("SetConfig message successfully prepared")
    }
  }
}

/**
 *
 * Aggregate all bot's information associated with one user.
 * id - user Id
 *
 */
class ActorsPackage(system: ActorSystem, id: Long) {

  val bots = new java.util.concurrent.ConcurrentHashMap[Long, BotSupporter]()

  def createActor(supporter: BotSupporter) = {
    bots.put(supporter.id, supporter)
    val ref = system.actorOf(Props(supporter.createActor), name = supporter.id.toString)
    supporter.setRef(ref)
    ref
  }

  def remove(bid: Long): Option[ActorRef] = bots.get(bid) match {
    case supporter: BotSupporter => {
      supporter.ref match {
        case Some(ref) => {
          ref ! Remove
          bots.remove(bid)
          Some(ref)
        }
        case _ => None
      }
    }
    case _ => None
  }

  def removeAll = bots.keys().foreach { bid => remove(bid) }

  def removeAll(sid: Int) = bots.keys().foreach { bid => remove(bid) }

  def bot(bid: Long) = bots.get(bid)

  def actor(bid: Long) = bots.get(bid).ref

  def botsCount(filter: (String, String)*): Long = bots.size()

  def botsList = bots.values().toList

  def botsPagesCount(filter: (String, String)*) = {
    val rowCount = botsCount(filter: _*)
    if (rowCount % Application.PAGE_SIZE > 0) rowCount / Application.PAGE_SIZE + 1 else rowCount / Application.PAGE_SIZE
  }

  def botsPage(pageNumber: Int, filter: (String, String)*) = {
    val beforeItems = if (pageNumber > 0) (pageNumber - 1) * Application.PAGE_SIZE else 0
    val afterItems = beforeItems + Application.PAGE_SIZE + 1
    botsList.slice(if (beforeItems > bots.size()) bots.size() else beforeItems, if (afterItems > bots.size()) bots.size() else afterItems)
  }

}

object BotController {

  var botCounter = 0

  def counter = {
    botCounter += 1
    botCounter
  }

  val system = ActorSystem("botSystem")

  val descrs = new java.util.concurrent.ConcurrentHashMap[String, BotDescriptor]()

  val botActors = new java.util.concurrent.ConcurrentHashMap[Long, ActorsPackage]()

  def botsList = descrs.values().toList

  def botsCount(filter: (String, String)*): Long = descrs.size()

  def botDescriptor(name: String): Option[BotDescriptor] = if (descrs.get(name) == null) None else Some(descrs.get(name))

  def botsPagesCount(filter: (String, String)*) = {
    val rowCount = botsCount(filter: _*)
    if (rowCount % Application.PAGE_SIZE > 0) rowCount / Application.PAGE_SIZE + 1 else rowCount / Application.PAGE_SIZE
  }

  def botsPage(pageNumber: Int, filter: (String, String)*) = {
    val beforeItems = if (pageNumber > 0) (pageNumber - 1) * Application.PAGE_SIZE else 0
    val afterItems = beforeItems + Application.PAGE_SIZE + 1
    botsList.slice(if (beforeItems > descrs.size()) descrs.size() else beforeItems, if (afterItems > descrs.size()) descrs.size() else afterItems)
  }

  def actorsPackage(id: Long) = botActors.getOrElseUpdate(id, new ActorsPackage(system, id))

  def sendMsg(uid: Long, bid: Long, actionName: String) = MessageMatcher.message(actionName) map {
    case Remove => remove(uid, bid)
    case otherMsg => {
      Logger.debug("ActorBotSystem.sendMsg(" + uid + ", " + bid + ", " + actionName + "): sending message to actor...")
      actor(uid, bid) map { _ ! otherMsg }
      Logger.debug("ActorBotSystem.sendMsg(" + uid + ", " + bid + ", " + actionName + "): message has been sent to actor")
    }
  }

  def removeAll(id: Long, sid: Int) = actorsPackage(id).removeAll(sid)

  def removeAll(id: Long) = actorsPackage(id).removeAll

  def remove(id: Long, bid: Long) = actorsPackage(id).remove(bid)

  def actor(id: Long, bid: Long): Option[ActorRef] = actorsPackage(id).actor(bid)

  def createActor(id: Long, supporter: BotSupporter): ActorRef = actorsPackage(id).createActor(supporter)

  def bot(id: Long, bid: Long): BotSupporter = actorsPackage(id).bot(bid)

  def addBotDescriptor(descr: BotDescriptor) = descrs.put(descr.name, descr)

}