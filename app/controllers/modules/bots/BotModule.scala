package controllers.modules.bots

import play.api.mvc.Flash
import play.Logger
import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import models.User
import models.Param
import controllers.Application
import play.api.mvc.Controller
import play.api.i18n.Messages
import play.api.mvc.Action
import play.api.mvc.Result
import play.api.mvc.Request
import scala.concurrent.ExecutionContext
import play.api.mvc.AnyContent
import views.html
import scala.collection.JavaConversions._
import akka.util.Timeout
import akka.pattern.ask
import akka.pattern.pipe
import scala.util.{ Success, Failure }
import scala.concurrent.Await
import scala.concurrent.duration._
import controllers.CheckNormalUser
import akka.actor.ActorRef
import play.twirl.api.Html
import play.api.Play.current

object BotModule extends CheckNormalUser {

  def usersList = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => usersList()")
      Ok(html.modules.bots.usersList())
  }

  def usersPage(pageNumber: Int, filter: String) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => usersPage(" + pageNumber + ", " + filter + ")")
      Ok(html.modules.bots.usersFilteredPage(User.page(pageNumber, Application.assemblyFilter(filter): _*)))
  }

  def botsList(uid: Long) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => botsList(" + uid + ")")
      Ok(html.modules.bots.botsList(uid))
  }

  def botsPage(uid: Long, pageNumber: Int, filter: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => botsPage(" + uid + ", " + pageNumber + ", " + filter + ")")
      Ok(html.modules.bots.botsFilteredPage(uid, BotController.botsPage(pageNumber, Application.assemblyFilter(filter): _*)))
  }

  def userBotsList(uid: Long) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => userBotsList(" + uid + ")")
      Ok(html.modules.bots.userBotsList(uid))
  }

  def userBotsPage(uid: Long, pageNumber: Int, filter: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => userBotsPage(" + uid + ", " + pageNumber + ", " + filter + ")")
      Ok(html.modules.bots.userBotsFilteredPage(uid, BotController.actorsPackage(uid).botsPage(pageNumber, Application.assemblyFilter(filter): _*)))
  }

  def botCreate(uid: Long, name: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => botCreate(" + uid + ", " + name + ")")
      BotController.botDescriptor(name) match {
        case Some(botDescr) => Ok(botDescr.createPage(uid))
        case _              => BadRequest
      }
  }

  def botDescription(uid: Long, name: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => botDescription(" + uid + ", " + name + ")")
      BotController.botDescriptor(name) match {
        case Some(botDescr) => Ok(botDescr.descriptionPage(uid))
        case _              => BadRequest
      }
  }

  def botSave(uid: Long, name: String, api: InvolvedAPI)(implicit user: User, request: Request[AnyContent]) = {
    Logger.debug("user: " + user.email + " => userSave(" + uid + ", " + name + ")")
    BotController.botDescriptor(name) match {
      case Some(botDescr) => {
        val newConfigForm = botDescr.configForm.bindFromRequest()
        newConfigForm.fold(
          hasErrors = { form =>
            Redirect(controllers.modules.bots.routes.BotModule.botCreate(uid, name)).
              flashing(Flash(form.data) + ("error" -> Messages("validation.errors")))
          },
          success = { config =>
            BotController.createActor(uid, botDescr.create(api, config))
            Redirect(controllers.modules.bots.routes.BotModule.userBotsList(uid)).
              flashing("success" -> Messages("module.bots.bot.new.success", "\"" + botDescr.title + "\""))
          })
      }
      case _ => BadRequest
    }
  }

  def userBotInfo(uid: Long, bid: Long) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => userBotInfo(" + uid + ", " + bid + ")")
      val bot = BotController.bot(uid, bid)
      if (bot == null) BadRequest else {
        implicit val timeout = Timeout(10 seconds)
        val future = bot.ref.get ? GetCurrentResult
        future onComplete {
          case Success(result) => result
          case Failure(error)  => ResultUnknown
        }
        var result: Any = ResultUnknown
        try {
          result = Await result (future, timeout.duration)
        } catch {
          case ex: Exception => {}
        }
        Ok(bot.descr.resultPage(result))
      }
  }

  def userBotAction(uid: Long, bid: Long, actionName: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => userBotAction(" + uid + ", " + bid + ", " + actionName + ")")
      BotController.sendMsg(uid, bid, actionName)
      Logger.debug("user: " + user.email + " => userBotAction: message has been sent")
      Redirect(controllers.modules.bots.routes.BotModule.userBotsList(uid))
  }
}
