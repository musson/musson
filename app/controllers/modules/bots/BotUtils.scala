package controllers.modules.bots

class SyncedValue[T](var value: T) {

  val lock = new Object()

  def get = lock.synchronized(value)

  def set(newValue: T) = lock.synchronized(value = newValue)

}

object MessageMatcher {

  def message(value: String) = if (value == null) None else
    value toLowerCase match {
      case "start"    => Some(Start)
      case "stop"     => Some(Stop)
      case "pause"    => Some(Pause)
      case "continue" => Some(Continue)
      case "remove"   => Some(Remove)
      case _          => None
    }

}

