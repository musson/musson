package controllers.modules.bots

import scala.concurrent.Await
import controllers.AppImplicits.appContext
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success
import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import play.api.Logger
import play.api.data.Form
import scala.collection.mutable.Stack

/**
 *
 * Messages to interact with BotProcessor executor (Actor).
 *
 */
abstract class Message
case class Start() extends Message
case class Stop() extends Message
case class Pause() extends Message
case class Continue() extends Message
case class Remove() extends Message

abstract class AskMessage extends Message
case class GetStatus() extends AskMessage
case class GetCurrentResult() extends AskMessage

abstract class StatusMessage extends Message
case class StatusProcessed() extends StatusMessage
case class StatusPaused() extends StatusMessage
case class StatusStopped() extends StatusMessage
case class StatusInStopProcess() extends StatusMessage
case class StatusReady() extends StatusMessage
case class StatusUnknown() extends StatusMessage

abstract class ResultMessage extends Message
case class ResultUnknown() extends ResultMessage
case class ResultCurrent(result: Any) extends ResultMessage

abstract class ConfigMessage extends Message
case class SetConfig(config: Any) extends ConfigMessage

trait InvolvedAPI

/**
 *
 * result type R, and API type A.
 *
 * BotProcessor used to calculate some result step by step
 * with using InvolvedAPI.
 *
 * Methods used by external executor (Actor) like this:
 *
 * ...
 * while(!isFinished) {
 *   tick
 *   Thread.sleep(isFinished)
 * }
 * ...
 *
 * And, of course at any time external executor can call
 * immutableResult.
 *
 *
 */
abstract class BotProcessor(api: InvolvedAPI, val category: String) {

  val id = BotController.counter

  def tick

  def isFinished: Boolean

  def immutableResult: Any

  def pause

  def configure(conf: Any)

}

object CommonBotPauses {

  val micropause = 1

  val timeLimitValue = 900000

}

object CommonBotStates {

  val STATE_ERROR_SKIP_ACTION_AND_CONTINUE = "Error state. Skip action and continue."

  val STATE_ERROR_TRY_ACTION_AGAIN = "Error state. Try action again."

  val STATE_MICROPAUSE = "State for micropause"

  val STATE_INITIAL = "Initial state"

  val STATE_UNKNOWN = "Unknown state"

  val STATE_FINISHED = "Finished state"

  val STATE_FINISHED_ON_ERROR = "Finished on error"

  val STATE_TIME_LIMIT = "Time limit"

  val STATE = "state"

  val RESULT = "result"

  val RESULTS = "results"

  val INFO = "info"

}

abstract class StateBotProcessor(api: InvolvedAPI, category: String) extends BotProcessor(api: InvolvedAPI, category: String) {

  var state = CommonBotStates.STATE_INITIAL

  var stateBefore = CommonBotStates.STATE_INITIAL

  var skipPause = false

  var finished = false

  val infoStack = new Stack[(String, Any)]

  def isFinished: Boolean = finished

  def logError(e: Throwable) = Logger.error("Exception: ", e)

  def prepareResult: Map[String, Any] = Map[String, Any](CommonBotStates.STATE -> state) + (CommonBotStates.INFO -> prepareStackInfo)

  def prepareStackInfo: Map[String, Any] = null

  def finish = {
    state(CommonBotStates.STATE_FINISHED)
    finished = true
  }

  def finishOnError(e: Throwable) = {
    logError(e)
    state(CommonBotStates.STATE_FINISHED_ON_ERROR)
    finished = true
  }

  def micropause = state(CommonBotStates.STATE_MICROPAUSE)

  def pushInfo(info: String, value: Any) = infoStack.push((info, value))

  def pushAndState(newState: String, value: Any) {
    pushInfo(newState, value)
    state(newState)
  }

  def pushAndFinishOnError(e: Throwable) {
    pushInfo(CommonBotStates.STATE_FINISHED_ON_ERROR, e)
    finishOnError(e)
  }

  def state(newState: String) {
    Logger.debug("Change state: " + this.getClass.getSimpleName + ": id: " + id + ", state: " + newState)
    stateBefore = state
    state = newState
  }

  def back = state(stateBefore)

  def backWithSkipPause = {
    skipPause = true
    state(stateBefore)
  }

  def selectPause: Long = state match {
    case CommonBotStates.STATE_MICROPAUSE => CommonBotPauses.micropause
    case CommonBotStates.STATE_FINISHED |
      CommonBotStates.STATE_FINISHED_ON_ERROR |
      CommonBotStates.STATE_UNKNOWN => 100000
    case CommonBotStates.STATE_INITIAL    => 10
    case CommonBotStates.STATE_TIME_LIMIT => CommonBotPauses.timeLimitValue
    case _                                => 100000
  }

  def tick = state match {
    case CommonBotStates.STATE_MICROPAUSE | CommonBotStates.STATE_TIME_LIMIT => backWithSkipPause
    case _ => {}
  }

  def pause = if (skipPause) skipPause = false else Thread.sleep(selectPause)

  def configure(conf: Any) = {}

}

/**
 *
 * Aggregate some common information and provide service methods to interact
 * between:
 *  - concrete instance of BotProcessor
 *  - common BotDescriptor for this type of BotProcessor
 *  - executor instance (Actor) for current BotProcessor
 *
 * Contains some useful methods, which process raw data from executor (Actor).
 * For example, executor answer for all messages returns Future. It's not useful to using
 * in templates without further processing and getting required simple result (see status method).
 *
 */
class BotSupporter(val id: Long, val descr: BotDescriptor, bot: BotProcessor) {

  var ref: Option[ActorRef] = None

  def this(descr: BotDescriptor, bot: BotProcessor) = this(bot.id, descr, bot)

  def setRef(inRef: ActorRef) { ref = Some(inRef) }

  def createActor = new BotActor(bot)

  def status: Any = {
    ref match {
      case Some(actorRef) => {
        implicit val timeout = Timeout(10 seconds)
        val future = (actorRef ? GetStatus)
        future onComplete {
          case Success(result) => result
          case Failure(error)  => StatusUnknown
        }
        return Await result (future, timeout.duration)
      }
      case None => StatusUnknown
    }
    StatusUnknown
  }

}

/**
 *
 * Used only as single object!
 *
 * 1) Using to create one type of BotSupporter's.
 * 2) Collect static information about one type of Bot's. For example,
 *    it can be - bot description, bot type name
 *
 */
abstract class BotDescriptor {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User): play.twirl.api.Html

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User): play.twirl.api.Html

  def descriptionPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User): play.twirl.api.Html

  def configForm: Form[_]

  def name: String

  def title: String

  def descr: String

  def create(api: InvolvedAPI, config: Any): BotSupporter

}