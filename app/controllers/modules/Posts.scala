package controllers.modules

import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import controllers.Datetime
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Content
import models.Permission
import play.api.data.Form
import play.api.data.Forms.date
import play.api.data.Forms.default
import play.api.data.Forms.ignored
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import play.api.i18n.Messages
import play.api.mvc.Controller
import play.api.mvc.Flash
import views.html
import controllers.Application

object Posts extends Controller with AsyncAuth with AuthConfigImpl {

  private val postForm: Form[Content] = Form(
    mapping(
      "id" -> default(longNumber, 0L),
      "ownerId" -> default(longNumber, 0L),
      "parentId" -> ignored(Option.empty[Long]),
      "title" -> optional(nonEmptyText),
      "data" -> optional(nonEmptyText),
      "status" -> default(optional(text), Some(Content.STATE_PUBLISHED)),
      "kind" -> ignored(Content.KIND_POST),
      "role" -> ignored(Content.ROLE_NO),
      "mimeType" -> ignored(Option.empty[String]),
      "created" -> default(date, Datetime.dtCurrentUTC),
      "modified" -> default(optional(date), None))(Content.apply)(Content.unapply))

  def show = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Redirect(controllers.modules.routes.Posts.list)
  }

  def list = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.posts.list())
  }

  def create = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val form = if (request2flash.get("error").isDefined)
        postForm.bind(request2flash.data)
      else
        postForm
      Ok(html.modules.posts.create(form))
  }

  def remove(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Content.delete(id) match {
        case 1L => Redirect(controllers.modules.routes.Posts.list).
          flashing("success" -> Messages("module.posts.remove.success", "\"" + id + "\""))
        case _ => BadRequest
      }
  }

  def edit(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      if (request2flash.get("error").isDefined) {
        Ok(html.modules.posts.edit(postForm.bind(request2flash.data), id))
      }
      Content.findById(id).map { post =>
        Ok(html.modules.posts.edit(postForm.fill(post), id))
      }.getOrElse(BadRequest)
  }

  def save = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val newPostForm = postForm.bindFromRequest()
      newPostForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Posts.create).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { newPost =>
          newPost.ownerId = user.id
          Content.create(newPost).map { createdPost =>
            Redirect(controllers.modules.routes.Posts.list).
              flashing("success" -> Messages("module.posts.new.success", "\"" + createdPost.title.get + "\""))
          }.getOrElse(BadRequest)
        })
  }

  def update(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val updatePostForm = postForm.bindFromRequest()
      updatePostForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Posts.edit(id)).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { updatedPost =>
          updatedPost.id = id
          Content.findById(id).map { origPost =>
            updatedPost.ownerId = origPost.ownerId
            Content.update(updatedPost)
            Redirect(controllers.modules.routes.Posts.list).
              flashing("success" -> Messages("module.posts.update.success", "\"" + updatedPost.title.get + "\""))
          }.getOrElse(BadRequest)
        })
  }

  def page(pageNumber: Int, filter: String) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.posts.filteredPage(Content.page(pageNumber, Application.assemblyFilter(filter, "kind" -> Content.KIND_POST): _*)))
  }

}
