package controllers.modules

import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import controllers.Datetime
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Content
import models.Permission
import play.api.data.Form
import play.api.data.Forms.date
import play.api.data.Forms.default
import play.api.data.Forms.ignored
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import play.api.i18n.Messages
import play.api.mvc.Controller
import play.api.mvc.Flash
import views.html
import controllers.Application

object Pages extends Controller with AsyncAuth with AuthConfigImpl {

  private val pageForm: Form[Content] = Form(
    mapping(
      "id" -> default(longNumber, 0L),
      "ownerId" -> default(longNumber, 0L),
      "parentId" -> ignored(Option.empty[Long]),
      "title" -> nonEmptyText,
      "data" -> optional(nonEmptyText),
      "status" -> default(optional(text), Some(Content.STATE_PUBLISHED)),
      "kind" -> ignored(Content.KIND_PAGE),
      "role" -> ignored(Content.ROLE_NO),
      "mimeType" -> ignored(Option.empty[String]),
      "created" -> default(date, Datetime.dtCurrentUTC),
      "modified" -> default(optional(date), None))(Content.apply)(Content.extract))

  def show = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Redirect(controllers.modules.routes.Pages.list)
  }

  def list = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.pages.list())
  }

  def create = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val form = if (request2flash.get("error").isDefined)
        pageForm.bind(request2flash.data)
      else
        pageForm
      Ok(html.modules.pages.create(form))
  }

  def remove(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Content.delete(id) match {
        case 1L => Redirect(controllers.modules.routes.Pages.list).
          flashing("success" -> Messages("module.pages.remove.success", "\"" + id + "\""))
        case _ => BadRequest
      }
  }

  def edit(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      if (request2flash.get("error").isDefined) {
        Ok(html.modules.pages.edit(pageForm.bind(request2flash.data), id))
      }
      Content.findById(id).map { page =>
        Ok(html.modules.pages.edit(pageForm.fill(page), id))
      }.getOrElse(BadRequest)
  }

  def save = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val newPageForm = pageForm.bindFromRequest()
      newPageForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Pages.create).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { newPage =>
          newPage.ownerId = user.id
          Content.create(newPage).map { createdPage =>
            Redirect(controllers.modules.routes.Pages.list).
              flashing("success" -> Messages("module.pages.new.success", "\"" + createdPage.title.get + "\""))
          }.getOrElse(BadRequest)
        })
  }

  def update(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val updatePageForm = pageForm.bindFromRequest()
      updatePageForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Pages.edit(id)).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { updatedPage =>
          updatedPage.id = id
          Content.findById(id).map { origPage =>
            updatedPage.ownerId = origPage.ownerId
            Content.update(updatedPage)
            Redirect(controllers.modules.routes.Pages.list).
              flashing("success" -> Messages("module.pages.update.success", "\"" + updatedPage.title.get + "\""))
          }.getOrElse(BadRequest)
        })
  }

  def page(pageNumber: Int, filter: String) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.pages.filteredPage(Content.page(pageNumber, Application.assemblyFilter(filter, "kind" -> Content.KIND_PAGE): _*)))
  }

}
