package controllers.modules

import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import play.api.mvc.Controller
import views.html

object Main extends Controller with AsyncAuth with AuthConfigImpl {

  def show = authorizedAction(Permission.normalUser) { implicit user =>
    implicit request =>
      if (user.permission == Permission.administartor) {
        Ok(html.modules.main.main())
      } else {
        Redirect(controllers.modules.bots.routes.BotModule.userBotsList(user.id))
      }
  }

}
