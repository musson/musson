package controllers.modules

import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import controllers.Datetime
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import models.Content
import models.Param
import play.api.data.Form
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.longNumber
import play.api.data.Forms.optional
import play.api.data.Forms.mapping
import play.api.data.Forms.default
import play.api.data.Forms.boolean
import play.api.data.Forms.text
import play.api.data.Forms.date
import play.api.mvc.Controller
import play.api.mvc.Flash
import play.api.i18n.Messages
import play.Logger
import views.html

case class BaseSettings(timezone: String, projectName: String, projectDescr: String, loginIsVisible: Boolean, registerIsActive: Boolean) {
  def update = {
    Param.update(Param.PARAM_TIMEZONE, timezone) &
      Param.update(Param.PARAM_PROJECT_DESCRIPTION, projectDescr) &
      Param.update(Param.PARAM_PROJECT_NAME, projectName) &
      Param.update(Param.PARAM_LOGIN_PAGE_VISIBLE, loginIsVisible.toString) &
      Param.update(Param.PARAM_REGISTER_IS_ACTIVE, registerIsActive.toString)
  }
}

object BaseSettings {

  def isActiveRegister = Param.findByName(Param.PARAM_REGISTER_IS_ACTIVE) match {
    case Some(param) => param.value.toBoolean
    case _           => Param.PARAM_VALUE_DEFAULT_REGISTER_IS_ACTIVE
  }

  def isVisibleLogin = Param.findByName(Param.PARAM_LOGIN_PAGE_VISIBLE) match {
    case Some(param) => param.value.toBoolean
    case _           => Param.PARAM_VALUE_DEFAULT_LOGIN_PAGE_VISIBLE
  }

  def load: Option[BaseSettings] = {
    Param.findByName(Param.PARAM_TIMEZONE).map { paramTimezone =>
      Param.findByName(Param.PARAM_PROJECT_NAME).map { paramProjectName =>
        Param.findByName(Param.PARAM_PROJECT_DESCRIPTION).map { paramProjectDescr =>
          Param.findByName(Param.PARAM_LOGIN_PAGE_VISIBLE).map { isVisibleLogin =>
            Param.findByName(Param.PARAM_REGISTER_IS_ACTIVE).map { registerIsActive =>
              Option(BaseSettings(paramTimezone.value, paramProjectName.value, paramProjectDescr.value, isVisibleLogin.value.toBoolean, registerIsActive.value.toBoolean))              
            }.getOrElse(None)
          }.getOrElse(None)
        }.getOrElse(None)
      }.getOrElse(None)
    }.getOrElse(None)
  }
}

case class IncludesSettings(includesHeader: Option[String], includesBody: Option[String], includesFooter: Option[String]) {

  def update(kind: String, data: Option[String]): Boolean = IncludesSettings.find(kind) match {
    case Some(content) => {
      content.data = data
      if (Content.update(content) > 0) true else false
    }
    case _ => Content.create(Content(2, None, data, None, kind, Content.ROLE_NO, None)) match {
      case Some(createdContent) => true
      case _                    => false
    }
  }

  def update: Boolean = update(Content.KIND_INCLUDE_HEADER, includesHeader) & update(Content.KIND_INCLUDE_BODY, includesBody) & update(Content.KIND_INCLUDE_FOOTER, includesFooter)
}

object IncludesSettings {

  def find(kind: String) = Content.findUnique("kind" -> kind)

  def findHeader = find(Content.KIND_INCLUDE_HEADER)

  def findBody = find(Content.KIND_INCLUDE_BODY)

  def findFooter = find(Content.KIND_INCLUDE_FOOTER)

  def header = findHeader match {
    case Some(content) => content.data.getOrElse("")
    case _             => ""
  }

  def body = findBody match {
    case Some(content) => content.data.getOrElse("")
    case _             => ""
  }

  def footer = findFooter match {
    case Some(content) => content.data.getOrElse("")
    case _             => ""
  }

  def load: IncludesSettings = IncludesSettings(
    findHeader.flatMap { content => content.data },
    findBody.flatMap { content => content.data },
    findFooter.flatMap { content => content.data })
}

object Settings extends Controller with AsyncAuth with AuthConfigImpl {

  private val includesSettingForm: Form[IncludesSettings] = Form(
    mapping(
      "includesHeader" -> optional(nonEmptyText),
      "includesBody" -> optional(nonEmptyText),
      "includesFooter" -> optional(nonEmptyText))(IncludesSettings.apply)(IncludesSettings.unapply))

  private val baseSettingsForm: Form[BaseSettings] = Form(
    mapping(
      "timezone" -> nonEmptyText,
      "projectName" -> nonEmptyText,
      "projectDescr" -> nonEmptyText,
      "loginIsVisible" -> boolean,
      "registerIsActive" -> boolean)(BaseSettings.apply)(BaseSettings.unapply))

  def show = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Redirect(controllers.modules.routes.Settings.base)
  }

  def media = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.settings.media())
  }

  def includes = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      if (request2flash.get("error").isDefined)
        Ok(html.modules.settings.includes(includesSettingForm.bind(request2flash.data)))
      else Ok(html.modules.settings.includes(includesSettingForm.fill(IncludesSettings.load)))
  }

  def includesSave = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Logger.debug("includeSave()")
      val newIncludesSettingsForm = includesSettingForm.bindFromRequest()
      newIncludesSettingsForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Settings.includes).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { includesSettings =>
          if (includesSettings.update) {
            Redirect(controllers.modules.routes.Settings.includes).
              flashing("success" -> Messages("module.settings.includes.update.success"))
          } else BadRequest
        })
  }

  def base = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      if (request2flash.get("error").isDefined)
        Ok(html.modules.settings.base(baseSettingsForm.bind(request2flash.data)))
      else {
        BaseSettings.load match {
          case Some(baseSettings) => Ok(html.modules.settings.base(baseSettingsForm.fill(baseSettings)))
          case None               => BadRequest
        }
      }
  }

  def baseSave = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      val newBaseSettingsForm = baseSettingsForm.bindFromRequest()
      newBaseSettingsForm.fold(
        hasErrors = { form =>
          Redirect(controllers.modules.routes.Settings.base).
            flashing(Flash(form.data) +
              ("error" -> Messages("validation.errors")))
        },
        success = { baseSettings =>
          if (baseSettings.update) {
            Redirect(controllers.modules.routes.Settings.base).
              flashing("success" -> Messages("module.settings.base.update.success"))
          } else BadRequest
        })
  }

}
