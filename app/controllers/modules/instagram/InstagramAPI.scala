package controllers.modules.instagram

import scala.collection.JavaConversions._
import org.jinstagram.Instagram
import org.jinstagram.http.Verbs
import org.jinstagram.model.Methods
import org.jinstagram.entity.tags.TagMediaFeed
import java.lang.reflect.Method
import org.apache.commons.lang3.StringUtils
import org.jinstagram.model.QueryParam
import org.jinstagram.InstagramObject
import org.jinstagram.exceptions.InstagramException
import org.jinstagram.entity.users.basicinfo.UserInfo
import org.jinstagram.http.Response
import org.jinstagram.InstagramConfig
import org.jinstagram.auth.model.OAuthRequest
import org.jinstagram.auth.model.Token
import java.util.concurrent.TimeUnit
import org.jinstagram.auth.model.OAuthConstants
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.entity.likes.LikesFeed
import scala.Int
import controllers.modules.bots.InvolvedAPI
import org.jinstagram.entity.media.MediaInfoFeed
import org.jinstagram.entity.tags.TagInfoData
import org.jinstagram.entity.common.User
import org.jinstagram.model.Relationship
import org.jinstagram.entity.users.basicinfo.UserInfoData
import play.Logger
import org.jinstagram.entity.common.Pagination
import models.FollowedBefore

class InstagramAPI(instagram: Instagram) extends InvolvedAPI {

  private object Locker

  def getUserId: String = Locker.synchronized { instagram.getCurrentUserInfo.getData.getId }

  def getUserIdByName(name: String): String = Locker.synchronized {
    val userList = instagram.searchUser(name).getUserList
    if (userList.size() > 0) {
      val userFeedData = userList.get(0)
      if (userFeedData != null && userFeedData.getUserName.equals(name)) {
        return userFeedData.getId
      }
    }
    return null
  }

  // can be recursive waiting after limit exception
  def getCleanFollowersFromUserLimitedByRequest(myUserId: String, userId: String, limit: Int): List[(String, String)] = Locker.synchronized {
    val myUsers = _getSubscribersFromUserLimitedByRequestCount(myUserId, limit / 3) ++ _getSubscribersFromUserLimitedByRequestCount(myUserId, limit / 3)
    _getFollowersFromUserLimitedByRequestCount(userId, limit / 3).filter(entry => !myUsers.contains(entry._1)).toList
  }

  private def _getSubscribersFromUser(userId: String, pagination: Pagination): (List[(String, String)], Pagination) = {
    val usersList = if (pagination == null) instagram.getUserFollowedByList(userId) else instagram.getUserFollowedByListNextPage(pagination)
    (usersList.getUserList.map { userFeedData => (userFeedData.getId, userFeedData.getUserName) } toList, usersList.getPagination)
  }

  def getFilteredSubscribersFromUser(userId: String, pagination: Pagination): (List[(String, String)], Pagination) = Locker.synchronized {
    val subscribersFromUser = _getSubscribersFromUser(userId, pagination)
    val filteredSubscribers = subscribersFromUser._1.filterNot(value =>
      instagram.getUserRelationship(value._1).getData.getOutgoingStatus.equals("follows") || // May be followed-by ???
        instagram.getUserRelationship(value._1).getData.getOutgoingStatus.equals("follower"))
    (filteredSubscribers, subscribersFromUser._2)
  }

  def getSubscribersFromUser(userId: String, pagination: Pagination): (List[(String, String)], Pagination) = Locker.synchronized { _getSubscribersFromUser(userId, pagination) }

  private def _getSubscribersFromUserLimitedByUsersCount(userId: String, limit: Int) = {
    val followList = instagram.getUserFollowedByList(userId)
    val usersList = followList.getUserList
    var nextPage = instagram.getUserFollowedByListNextPage(followList.getPagination)
    while (nextPage.getPagination != null && usersList.length < limit) {
      usersList.addAll(nextPage.getUserList)
      nextPage = instagram.getUserFollowedByListNextPage(nextPage.getPagination)
    }
    usersList.map { userFeedData => (userFeedData.getId -> userFeedData.getUserName) } toMap
  }

  private def _getSubscribersFromUserLimitedByRequestCount(userId: String, limit: Int) = {
    val followList = instagram.getUserFollowedByList(userId)
    val usersList = followList.getUserList
    var nextPage = instagram.getUserFollowedByListNextPage(followList.getPagination)
    var counter = 2
    while (nextPage.getPagination != null && counter < limit) {
      usersList.addAll(nextPage.getUserList)
      nextPage = instagram.getUserFollowedByListNextPage(nextPage.getPagination)
      counter += 1
    }
    usersList.map { userFeedData => (userFeedData.getId -> userFeedData.getUserName) } toMap
  }

  private def _getFollowersFromUserLimitedByRequestCount(userId: String, limit: Int) = {
    val followList = instagram.getUserFollowList(userId)
    val usersList = followList.getUserList
    var nextPage = instagram.getUserFollowListNextPage(followList.getPagination)
    var counter = 2
    while (nextPage.getPagination != null && counter < limit) {
      usersList.addAll(nextPage.getUserList)
      nextPage = instagram.getUserFollowListNextPage(nextPage.getPagination)
      counter += 1
    }
    usersList.map { userFeedData => (userFeedData.getId -> userFeedData.getUserName) } toMap
  }

  def getTopSubscribersList(uid: String) = Locker.synchronized {
    val followList = instagram.getUserFollowList(uid)
    val usersList = followList.getUserList
    var nextPage = instagram.getUserFollowedByListNextPage(followList.getPagination)
    while (nextPage.getPagination != null && usersList.size() < 250) {
      usersList.addAll(nextPage.getUserList)
      nextPage = instagram.getUserFollowedByListNextPage(nextPage.getPagination)
    }
    usersList.map { userFeedData => (userFeedData.getId, userFeedData.getUserName) }
  }

  def removeSubscriber(userId: String): String = Locker.synchronized {
    if (instagram.getUserRelationship(userId).getData.getOutgoingStatus.equals("follows")) {
      instagram.setUserRelationship(userId, Relationship.UNFOLLOW)
      "success"
    } else {
      "before"
    }
  }

  def getRecentMediaTagForms(tagForm: String): List[String] = Locker.synchronized { for (tagInfoData <- instagram.searchTags(tagForm).getTagList.toList) yield tagInfoData.getTagName }

  def getRecentMediaByTag(tagName: String): List[String] = Locker.synchronized { for (mediaFeed <- instagram.getRecentMediaTags(tagName, 200).getData.toList) yield mediaFeed.getId }

  def like(mediaId: String) = Locker.synchronized {
    val mediaFeedData = instagram.getMediaInfo(mediaId).getData
    if (mediaFeedData.isUserHasLiked()) {
      "before"
    } else {
      instagram.setUserLike(mediaId)
      "success"
    }
  }

  def getRecentSelfMediaFeed(uid: String): List[(String, String)] = Locker.synchronized {
    for (mediaFeed <- instagram.getUserFeeds.getData.toList) yield {
      mediaFeed.getUser match {
        case user: User => user.getId.equals(uid) match {
          case false => (mediaFeed.getId, mediaFeed.getUser.getUserName)
        }
      }
    }
  }

  def getRecentUsersByTag(tagName: String): List[(String, String)] = Locker.synchronized(for (mediaFeed <- instagram.getRecentMediaTags(tagName, 200).getData.toList) yield (mediaFeed.getUser.getId -> mediaFeed.getUser.getUserName)) distinct

  def setSubscriberIfNot(accountId: String, userId: String) = Locker.synchronized {
    if (instagram.getUserRelationship(userId).getData.getOutgoingStatus.equals("follows")) {
      "before"
    } else {
      instagram.setUserRelationship(userId, Relationship.FOLLOW)
      FollowedBefore.createIfNotExists(userId.toLong, accountId.toLong)
      "success"
    }
  }

  def setSetLikesAndSetSubscriberIfNot(accountId: String, userId: String, likesNumber: Int) = Locker.synchronized {
    if (instagram.getUserRelationship(userId).getData.getOutgoingStatus.equals("follows")) {
      "before";
    } else {
      for (mediaFeedData <- instagram.getRecentMediaFeed(userId, likesNumber, null, null, null, null).getData) {
        if (!mediaFeedData.isUserHasLiked()) {
          instagram.setUserLike(mediaFeedData.getId)
        }
      }
      instagram.setUserRelationship(userId, Relationship.FOLLOW)
      FollowedBefore.createIfNotExists(userId.toLong, accountId.toLong)
      "success"
    }
  }

  def getUserInfo: Map[String, String] = Locker.synchronized {
    val counts = instagram.getCurrentUserInfo.getData.getCounts
    Map("followers" -> counts.getFollows.toString(),
      "subscribers" -> counts.getFollwed_by.toString(),
      "media_count" -> counts.getMedia.toString())
  }

}
