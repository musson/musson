package controllers.modules.instagram

import play.Logger
import play.twirl.api.Html
import play.api.Play.current
import play.api.mvc.Controller
import play.api.mvc.AnyContent
import play.api.mvc.Request
import play.api.mvc.Action
import play.api.mvc.Flash
import play.api.mvc.Result
import play.api.i18n.Messages
import jp.t2v.lab.play2.auth.AsyncAuth
import models.InstagramToken
import models.Permission
import models.Param
import models.User
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import org.jinstagram.auth.oauth.InstagramService
import org.jinstagram.auth.InstagramAuthService
import org.jinstagram.auth.model.Verifier
import org.jinstagram.auth.model.Token
import scala.concurrent.ExecutionContext
import views.html
import akka.pattern.ask
import akka.pattern.pipe
import akka.actor.ActorRef
import akka.util.Timeout
import scala.util.{ Success, Failure }
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.collection.JavaConversions._
import controllers.Application
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.MessageMatcher
import controllers.modules.bots.BotController
import controllers.modules.bots.BotModule
import controllers.modules.bots.GetCurrentResult
import controllers.modules.bots.ResultUnknown
import controllers.modules.bots.Continue
import controllers.modules.bots.Stop
import controllers.modules.bots.Remove
import controllers.modules.bots.Pause
import controllers.modules.bots.Start
import controllers.modules.instagram.bots.SingleConcurrentRelationsLikerBot1Descr
import controllers.modules.instagram.bots.SingleConcurrentRelationsBot1Descr
import controllers.modules.instagram.bots.RelationsLikerBot1Descr
import controllers.modules.instagram.bots.SelfFeedLikerBot1Descr
import controllers.modules.instagram.bots.RelationsBot1Descr
import controllers.modules.instagram.bots.TagsLikerBot1Descr
import controllers.modules.instagram.bots.RemoverBot1Descr
import controllers.modules.instagram.bots.LikerBot1Descr
import controllers.modules.instagram.bots.StatsBot1Descr
import controllers.AppImplicits.appContext
import controllers.CheckNormalUser
import controllers.AuthConfigImpl

object InstagramParamsInitializer {

  val PARAM_INSTAGRAM_API_1_HOST_1 = "instagram_api_1_host_1"

  val PARAM_INSTAGRAM_API_1_KEY_1 = "instagram_api_1_key_1"

  val PARAM_INSTAGRAM_API_1_SECRET_1 = "instagram_api_1_secret_1"

  val PARAM_INSTAGRAM_API_1_IP_1 = "instagram_api_1_IP_1"

  def init = {
    Seq(
      Param(PARAM_INSTAGRAM_API_1_HOST_1, current.configuration.getString(PARAM_INSTAGRAM_API_1_HOST_1).get),
      Param(PARAM_INSTAGRAM_API_1_IP_1, current.configuration.getString(PARAM_INSTAGRAM_API_1_IP_1).get),
      Param(PARAM_INSTAGRAM_API_1_KEY_1, current.configuration.getString(PARAM_INSTAGRAM_API_1_KEY_1).get),
      Param(PARAM_INSTAGRAM_API_1_SECRET_1, current.configuration.getString(PARAM_INSTAGRAM_API_1_SECRET_1).get)).foreach(Param.create)
  }

  def appAdress = Param.findByName(InstagramParamsInitializer.PARAM_INSTAGRAM_API_1_HOST_1).get.value

  def apiKey = Param.findByName(InstagramParamsInitializer.PARAM_INSTAGRAM_API_1_KEY_1).get.value

  def apiSecret = Param.findByName(InstagramParamsInitializer.PARAM_INSTAGRAM_API_1_SECRET_1).get.value

  def apiIP = Param.findByName(InstagramParamsInitializer.PARAM_INSTAGRAM_API_1_IP_1).get.value

}

object InstagramModule extends CheckNormalUser {

  {
    BotController.addBotDescriptor(SingleConcurrentRelationsLikerBot1Descr)
    BotController.addBotDescriptor(SingleConcurrentRelationsBot1Descr)
    BotController.addBotDescriptor(RemoverBot1Descr)
    BotController.addBotDescriptor(LikerBot1Descr)
    BotController.addBotDescriptor(SelfFeedLikerBot1Descr)
    BotController.addBotDescriptor(RelationsBot1Descr)
    BotController.addBotDescriptor(StatsBot1Descr)
    BotController.addBotDescriptor(RelationsLikerBot1Descr)
    BotController.addBotDescriptor(TagsLikerBot1Descr)
  }

  val SCOPE_COMMENTS = "comments"

  val SCOPE_LIKES = "likes"

  val SCOPE_REALTIONSHIPS = "relationships"

  val SCOPE_ALL = SCOPE_COMMENTS + " " + SCOPE_LIKES + " " + SCOPE_REALTIONSHIPS

  val services = new java.util.concurrent.ConcurrentHashMap[Long, java.util.concurrent.ConcurrentHashMap[Int, InstagramService]]()

  val instagrams = new java.util.concurrent.ConcurrentHashMap[Long, java.util.concurrent.ConcurrentHashMap[Int, InstagramAPI]]()

  def getService(uid: Long, sid: Int) = services.getOrElseUpdate(uid, new java.util.concurrent.ConcurrentHashMap[Int, InstagramService]).get(sid)

  def setService(uid: Long, sid: Int, service: InstagramService) = services.getOrElseUpdate(uid, new java.util.concurrent.ConcurrentHashMap[Int, InstagramService]()).put(sid, service)

  def getInstagram(uid: Long, sid: Int) = instagrams.getOrElseUpdate(uid, new java.util.concurrent.ConcurrentHashMap[Int, InstagramAPI]).get(sid)

  def setInstagram(uid: Long, sid: Int, api: InstagramAPI) = instagrams.getOrElseUpdate(uid, new java.util.concurrent.ConcurrentHashMap[Int, InstagramAPI]()).put(sid, api)

  def removeService(uid: Long, sid: Int) {
    val localServices = services.get(uid)
    if (localServices != null) {
      localServices.remove(sid)
    }
  }

  def removeInstagramsAll(uid: Long) {
    val local = instagrams.get(uid)
    if (local != null)
      local.clear()
  }

  def removeServicesAll(uid: Long) {
    val local = services.get(uid)
    if (local != null)
      local.clear()
  }

  def removeInstagramsAll() {
    instagrams.foreach(f => f._2.clear())
    instagrams.clear()
  }

  def removeServicesAll() {
    services.foreach(f => f._2.clear())
    services.clear()
  }

  def removeInstagram(uid: Long, sid: Int) {
    val localInstagrams = instagrams.get(uid)
    if (localInstagrams != null) {
      localInstagrams.remove(sid)
    }
  }

  def getOrElseUpdateService(uid: Long, sid: Int): InstagramService = {
    var service: InstagramService = getService(uid, sid)
    if (service == null) {
      service = getInstagramService(uid, sid)
      setService(uid, sid, service)
    }
    service
  }

  def getInstagramService(uid: Long, sid: Int) = new InstagramAuthService()
    .apiKey(InstagramParamsInitializer.apiKey)
    .apiSecret(InstagramParamsInitializer.apiSecret)
    .scope(SCOPE_ALL)
    .callback("http://" + InstagramParamsInitializer.appAdress + "/admin/modules/instagram/users/user/handleToken/" + sid + "/handler?uid=" + uid)
    .build()

  def instagramAuthorizedAction(authority: Authority, uid: Long, sid: Int)(f: User => (Request[AnyContent] => Result))(implicit context: ExecutionContext): Action[(AnyContent, User)] = {
    checkNormalUserAction(authority, uid)({ implicit user =>
      implicit request =>
        Logger.debug("user: " + user.email + " => instagramAuthorizedAction(" + authority + ", " + uid + ", " + sid + ")")
        if (getInstagram(uid, sid) == null) {
          InstagramToken.findToken(uid, sid) match {
            case Some(tokenFromDb) => Redirect(controllers.modules.instagram.routes.InstagramModule.handleToken(uid, sid, Some(tokenFromDb.code.get))) withSession (Application.RETURN_URL -> request.uri)
            case _                 => Redirect(getOrElseUpdateService(uid, sid).getAuthorizationUrl(null)) withSession (Application.RETURN_URL -> request.uri)
          }
        } else { f.apply(user)(request) }
    })
  }

  def handleToken(uid: Long, sid: Int, code: Option[String]) = checkNormalUserAction(Permission.administartor, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => handleToken(" + uid + ", " + sid + ", " + code.getOrElse("null") + ")")
      InstagramToken.findToken(uid, sid) match {
        case Some(tokenFromDb) => {
          setInstagram(uid, sid, new InstagramAPI(new org.jinstagram.Instagram(tokenFromDb.token.get, InstagramParamsInitializer.apiSecret, InstagramParamsInitializer.apiIP)))
          Redirect(request.session.get(Application.RETURN_URL).getOrElse(controllers.routes.Application.index.toString())) withSession (request.session - Application.RETURN_URL)
        }
        case _ => {
          val verifier: Verifier = new Verifier(code.get)
          val service = getService(uid, sid)
          if (service == null) {
            BadRequest
          } else {
            val token: Token = service.getAccessToken(null, verifier)
            setInstagram(uid, sid, new InstagramAPI(new org.jinstagram.Instagram(token.getToken, InstagramParamsInitializer.apiSecret, InstagramParamsInitializer.apiIP)))
            removeService(uid, sid)
            InstagramToken.create(InstagramToken(uid, 0, Some(code.get), Some(token.getToken), Some(token.getSecret), Some(token.getRawResponse))) match {
              case Some(createdToken) => Redirect(request.session.get(Application.RETURN_URL).getOrElse(controllers.routes.Application.index.toString())) withSession (request.session - Application.RETURN_URL)
              case _                  => BadRequest
            }
          }
        }
      }
  }

  def startInstagramModuleUsersList(uid: Long, sid: Int) = instagramAuthorizedAction(Permission.administartor, uid, sid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => startInstagramModuleUsersList(" + uid + ", " + sid + ")")
      Redirect(controllers.modules.bots.routes.BotModule.usersList())
  }
  
  def startInstagramModuleBotsList(uid: Long, sid: Int) = instagramAuthorizedAction(Permission.administartor, uid, sid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => startInstagramModuleBotsList(" + uid + ", " + sid + ")")
      Redirect(controllers.modules.bots.routes.BotModule.botsList(uid))
  }
  
  def startInstagramModuleUserBotsList(uid: Long, sid: Int) = instagramAuthorizedAction(Permission.administartor, uid, sid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => startInstagramModuleUserBotsList(" + uid + ", " + sid + ")")
      Redirect(controllers.modules.bots.routes.BotModule.userBotsList(uid))
  }

  def removeInstagramModuleAll(uid: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => removeInstagramModuleAll(" + uid + ")")
      BotController.removeAll(uid) // TODO: remove only modules with this instagram 
      removeInstagramsAll(uid)
      removeServicesAll(uid)
      // TODO: fin all tokens
      InstagramToken.findToken(uid, 0) match {
        case Some(token) => InstagramToken.delete(token.id)
        case _           => {}
      }
      Ok(html.modules.bots.usersList())
  }
  
  def removeInstagramModuleUserBotsList(uid: Long, sid: Int) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => removeInstagramModuleUserBotsList(" + uid + ", " + sid + ")")
      BotController.removeAll(uid, sid) // TODO: remove only modules with this instagram 
      removeInstagram(uid, sid)
      removeService(uid, sid)
      InstagramToken.findToken(uid, sid) match {
        case Some(token) => InstagramToken.delete(token.id)
        case _           => {}
      }
      Ok(html.modules.bots.userBotsList(uid))
  }

  def removeInstagramModule(uid: Long, sid: Int) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      Logger.debug("user: " + user.email + " => removeInstagramModule(" + uid + ", " + sid + ")")
      BotController.removeAll(uid, sid) // TODO: remove only modules with this instagram 
      removeInstagram(uid, sid)
      removeService(uid, sid)
      InstagramToken.findToken(uid, sid) match {
        case Some(token) => InstagramToken.delete(token.id)
        case _           => {}
      }
      Ok(html.modules.bots.usersList())
  }

  def botSave(uid: Long, name: String) = checkNormalUserAction(Permission.normalUser, uid) { implicit user =>
    implicit request =>
      BotModule.botSave(uid, name, getInstagram(uid, 0));
  }

}
