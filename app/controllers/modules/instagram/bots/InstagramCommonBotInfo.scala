package controllers.modules.instagram.bots

/**
 * ALL
 * 0)==============
 * java.lang.NullPointerException
 *
 * When instagram module not started
 * 1)==============
 * org.jinstagram.exceptions.InstagramBadRequestException: OAuthAccessTokenException: The access_token provided is invalid.
 *
 * sometimes on refresing access token on the instagram side, need fully reauthentification with user signs, and replace instagram module
 * 2)==============
 * org.jinstagram.exceptions.InstagramException: Unknown error response code: 429 {"meta":{"error_type":"OAuthRateLimitException","code":429,"error_message":"The maximum number of requests per hour has been exceeded. You have made 100 requests of the 100 allowed in the last hour."}}
 *
 * on likes, comments, followers
 * 3)==============
 * org.jinstagram.exceptions.InstagramBadRequestException: APINotFoundError: invalid media id
 *
 * my be on access denied for users
 *
 *
 *
 *
 * ----------
 * non-critical:
 * - InstagramBadRequestException
 *
 * critical -> to stop:
 * other
 *
 */
object InstagramCommonBotInfo {

  val COMMON_STATS_INFO = "CommonStatsInfo"

  // fields
  val ALL = "all"

  // statuses
  val STATUS = "status"

  val STATUS_BAD_REQUEST = "BadRequest"

  val STATUS_ACCESS_DENIED = "AccessDenied"

  val STATUS_SUCCESS = "success"

  val STATUS_BEFORE = "before"

  // constants

  val perHourLikesUnlikesOperationsLimit = 100

  val perHourRelationsOperationsLimit = 60

  val second = 1000

  val minute = 60 * second

  val hour = 60 * minute

  val day = 24 * hour

  val week = 7 * day

  // timers

  val TIME_READ_PAUSE = 100

  val TIME_CHANGE_LIKES_PAUSE = hour / perHourLikesUnlikesOperationsLimit

  val TIME_CHANGE_RELATIONS_PAUSE = hour / perHourRelationsOperationsLimit

  // common instagram states

  val STATE_GET_TAG_FORMS = "Getting tag forms"

  val STATE_GET_RECENT_POSTS = "Getting recent posts"

  val STATE_LIKE_POSTS = "Liking posts"

  val STATE_GET_USERS = "Getting users"

  val STATE_SET_RELATIONS = "Setting relations"

  val STATE_REMOVE_USERS = "Removing users"

  val STATE_COLLECT_INFO = "Collecting info"

  // other information

  val CATEGORY_ISNTAGRAM = "instagram"

  val STATE_APP_ERR_IGNORED = "Application error with skiping"

  val STATE_ACCESS_TOKEN_INVALID = "Access token invalidated"

  val STATE_IGNORED_WITH_CONTINUE_ERR = "Ignored with continue error"

  val timeLimitValueInvalidAccessToken = 15000

  val timeLimitValueIgnored = 60000

  val ER_MSG_1 = "APINotFoundError: invalid media id"

  val ER_MSG_2 = "Unknown error response code: 429 {\"meta\":{\"error_type\":\"OAuthRateLimitException\",\"code\":429,\"error_message\":\"The maximum number of requests per hour has been exceeded"

  val ER_MSG_3 = "OAuthAccessTokenException: The access_token provided is invalid"

  val ER_MSG_4 = "Sorry, something went wrong."

  val ER_MSG_5 = "Unknown error response code: 502"

  val ER_MSG_6 = "Service unavailable"

  val ER_MSG_7 = "APINotAllowedError: you cannot view this resource"

  val ER_MSG_8 = "503 Service Unavailable"

  val ER_MSG_9 = "500 Oops, an error occurred"

  val ER_MSG_10 = "APINotFoundError: this user does not exist" // InstagramBadRequestException

  val ER_MSG_11 = "APIError: This account can't be followed right now." // InstagramBadRequestException

  val ER_MSG_12 = "IOException while retrieving data" // InstagramException due to Connection refused or Connection timed out

  val ER_MSG_13 = "Cannot get String from a null object" // Exception - IllegalArgumentException - some problems on the API side<->Iserver

  val ER_MSG_14 = "APINotFoundError: this user does not exist" // InstagramBadRequestException

  val ER_MSG_15 = "Unknown error response code: 504" // InstagramException - gateway time out

  val IGNORED_ERR_WITH_CONTINUE = List(ER_MSG_4, ER_MSG_5, ER_MSG_6, ER_MSG_8, ER_MSG_9, ER_MSG_12, ER_MSG_15)

  val ACCESS_DENIED_ERR = List(ER_MSG_1, ER_MSG_5, ER_MSG_7, ER_MSG_10, ER_MSG_11, ER_MSG_14)

  val APP_ERR_IGNORED = List(ER_MSG_13)

}
