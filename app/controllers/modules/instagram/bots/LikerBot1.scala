package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses

case class LikerBot1Config(val dummy: Option[String], val tag: String)

class LikerBot1(api: InstagramAPI, descr: BotDescriptor, config: LikerBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var processedFormsIndex: Int = 0

  var processedFormsList: List[String] = null

  var processedPostsIndex: Int = 0

  var processedPostsList: List[String] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    case InstagramCommonBotInfo.STATE_GET_TAG_FORMS => commonCatched {
      processedFormsIndex = 0
      processedFormsList = api.getRecentMediaTagForms(config.tag)
      if (processedFormsList.length > 0) state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    }
    case InstagramCommonBotInfo.STATE_GET_RECENT_POSTS => if (processedFormsIndex < processedFormsList.length) commonCatched {
      processedPostsIndex = 0
      processedPostsList = api.getRecentMediaByTag(processedFormsList(processedFormsIndex))
      if (processedPostsList.length > 0) state(InstagramCommonBotInfo.STATE_LIKE_POSTS)
    }
    else state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    case InstagramCommonBotInfo.STATE_LIKE_POSTS =>
      if (processedPostsIndex < processedPostsList.length)
        commonChangeCatched(standartStatusPrepare(api.like(processedPostsList(processedPostsIndex))), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    processedData += Map(
      "local_index" -> processedPostsIndex.toString,
      "hash_tag" -> processedFormsList(processedFormsIndex),
      "media_id" -> processedPostsList(processedPostsIndex),
      "status" -> status)
    processedPostsIndex += 1
  }

}

object LikerBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState1(RelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.likerBot1.create(uid, configForm)

  def configForm: Form[LikerBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "tag" -> nonEmptyText)(LikerBot1Config.apply)(LikerBot1Config.unapply))

  def name = "likerbot1"

  def title = "Liker Bot 1 v0.1"

  def descr = "Лайкает недавние посты."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new LikerBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[LikerBot1Config]))

}

