package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.number
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses

case class StatsBot1Config(val dummy: Option[String], val period: Int)

class StatsBot1(api: InstagramAPI, descr: BotDescriptor, config: StatsBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => state(InstagramCommonBotInfo.STATE_COLLECT_INFO)
    case InstagramCommonBotInfo.STATE_COLLECT_INFO => commonCatched {
      processedData += api.getUserInfo ++ Map("time" -> System.currentTimeMillis().toString())
    }
    case _ => super.tick
  }

  override def selectPause = state match {
    case InstagramCommonBotInfo.STATE_COLLECT_INFO => config.period
    case _                                         => super.selectPause
  }

  override def immutableResult: Any = prepareResult

}

object StatsBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.statsBot1.state(StatsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.statsBot1.create(uid, configForm)

  def configForm: Form[StatsBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "period" -> number(InstagramCommonBotInfo.minute, InstagramCommonBotInfo.week))(StatsBot1Config.apply)(StatsBot1Config.unapply))

  def name = "statsbot1"

  def title = "Stats Bot 1 v0.1"

  def descr = "Собирает общую статистику."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new StatsBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[StatsBot1Config]))

  def maxValueVelocity(name: String, result: ListBuffer[Map[String, String]]): String = if (result.size > 0) {
    val value = result.maxBy(map => map(name).toDouble)
    (value(name).toDouble - result(0)(name).toDouble).toString
  } else 0.toString

  def minValueVelocity(name: String, result: ListBuffer[Map[String, String]]): String = 0.toString

  def maxValue(name: String, result: ListBuffer[Map[String, String]]): String = if (result.size > 0) {
    val value = result.maxBy(map => map(name).toDouble)
    value(name)
  } else 0.toString

  def minValue(name: String, result: ListBuffer[Map[String, String]]): String = 0.toString

  def maxValueX(result: ListBuffer[Map[String, String]]): String = result.size.toString

  def minValueX(result: ListBuffer[Map[String, String]]): String = 0.toString

  def buildGraphicDataForLine(name: String, result: ListBuffer[Map[String, String]]): String = (for ((map, index) <- result.zipWithIndex) yield "{\"x\": " + index + ", \"y\": " + map(name) + "}").mkString("[", ", ", "]")

  def buildGraphicDataVelocityForLine(name: String, result: ListBuffer[Map[String, String]]): String =
    (for ((map, index) <- result.zipWithIndex) yield "{\"x\": " + index + ", \"y\": " + ((map(name).toDouble - result(0)(name).toDouble) match {
      case some if some > 0 => some / index.toDouble
      case _                => 0
    }) + "}").mkString("[", ", ", "]")

}

