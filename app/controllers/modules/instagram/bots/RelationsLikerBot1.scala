package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses
import models.FollowedBefore

case class RelationsLikerBot1Config(val dummy: Option[String], val tag: String)

class RelationsLikerBot1(api: InstagramAPI, descr: BotDescriptor, config: RelationsLikerBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var accountId: String = null

  var processedFormsIndex: Int = 0

  var processedFormsList: List[String] = null

  var processedUsersIndex: Int = 0

  var processedUsersList: List[(String, String)] = null

  var limitCounter = 0

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => commonCatched {
      accountId = api.getUserId
      if (accountId == null) finish else state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    }
    case InstagramCommonBotInfo.STATE_GET_TAG_FORMS => commonCatched {
      processedFormsIndex = 0
      processedFormsList = api.getRecentMediaTagForms(config.tag)
      if (processedFormsList.length > 0) state(InstagramCommonBotInfo.STATE_GET_USERS)
    }
    case InstagramCommonBotInfo.STATE_GET_USERS => if (processedFormsIndex < processedFormsList.length) commonCatched {
      processedUsersIndex = 0
      processedUsersList = api.getRecentUsersByTag(processedFormsList(processedFormsIndex))
      if (processedUsersList.length > 0) state(InstagramCommonBotInfo.STATE_SET_RELATIONS)
    }
    else state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    case InstagramCommonBotInfo.STATE_SET_RELATIONS =>
      if (processedUsersIndex < processedUsersList.length) commonChangeCatched(
        standartStatusPrepare(api.setSetLikesAndSetSubscriberIfNot(accountId, processedUsersList(processedUsersIndex)._1, if (limitCounter < 40) 2 else 1)), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_USERS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    if (status.equals(InstagramCommonBotInfo.STATUS_SUCCESS)) FollowedBefore.createIfNotExists(accountId.toLong, processedUsersList(processedUsersIndex)._1.toLong)
    processedData += Map(
      "local_index" -> processedUsersIndex.toString,
      "user_id" -> processedUsersList(processedUsersIndex)._1,
      "user_name" -> processedUsersList(processedUsersIndex)._2,
      "status" -> status)
    processedUsersIndex += 1
    if (limitCounter < 60) limitCounter += 1 else limitCounter = 0
  }

}

object RelationsLikerBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState2(RelationsLikerBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.relationsLikerBot1.create(uid, configForm)

  def configForm: Form[RelationsLikerBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "tag" -> nonEmptyText)(RelationsLikerBot1Config.apply)(RelationsLikerBot1Config.unapply))

  def name = "relationslikerbot1"

  def title = "Relations Liker Bot 1 v0.1"

  def descr = "Ищет недавние посты по формам тэга. У постов берет пользователей, лайкает у него один-два поста и подписывается на него."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new RelationsLikerBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[RelationsLikerBot1Config]))

}
