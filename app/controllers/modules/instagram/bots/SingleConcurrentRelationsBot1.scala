package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses
import org.jinstagram.entity.common.Pagination
import models.FollowedBefore

case class SingleConcurrentRelationsBot1Config(val dummy: Option[String], val user: String)

class SingleConcurrentRelationsBot1(api: InstagramAPI, descr: BotDescriptor, config: SingleConcurrentRelationsBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var accountId: String = null

  var userId: String = null

  var pagination: Pagination = null

  var processedUsersIndex: Int = 0

  var processedUsersList: List[(String, String)] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => commonCatched {
      accountId = api.getUserId
      if (accountId == null) finish else {
        userId = api.getUserIdByName(config.user)
        if (userId == null) finish else state(InstagramCommonBotInfo.STATE_GET_USERS)
      }
    }
    case InstagramCommonBotInfo.STATE_GET_USERS => commonCatched {
      processedUsersIndex = 0
      val result = api.getSubscribersFromUser(userId, pagination)
      processedUsersList = result._1
      pagination = result._2
      if (processedUsersList.length > 0) state(InstagramCommonBotInfo.STATE_SET_RELATIONS) else finish
    }
    case InstagramCommonBotInfo.STATE_SET_RELATIONS =>
      if (processedUsersIndex < processedUsersList.length) commonChangeCatched(
        standartStatusPrepare(api.setSubscriberIfNot(accountId, processedUsersList(processedUsersIndex)._1)), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_USERS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    if (status.equals(InstagramCommonBotInfo.STATUS_SUCCESS)) FollowedBefore.createIfNotExists(userId.toLong, processedUsersList(processedUsersIndex)._1.toLong)
    processedData += Map(
      "local_index" -> processedUsersIndex.toString,
      "user_id" -> processedUsersList(processedUsersIndex)._1,
      "user_name" -> processedUsersList(processedUsersIndex)._2,
      "status" -> status)
    processedUsersIndex += 1
  }

}

object SingleConcurrentRelationsBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState2(SingleConcurrentRelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.singleConcurrentRelationsBot1.create(uid, configForm)

  def configForm: Form[SingleConcurrentRelationsBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "user" -> nonEmptyText)(SingleConcurrentRelationsBot1Config.apply)(SingleConcurrentRelationsBot1Config.unapply))

  def name = "singleconcurrentrelationsbot1"

  def title = "Single Concurrent Relations Bot 1 v0.1"

  def descr = "Подписывается на пользователей, которые подписаны на указанного пользователя."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new SingleConcurrentRelationsBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[SingleConcurrentRelationsBot1Config]))

}

