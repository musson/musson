package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses
import models.FollowedBefore

case class RelationsBot1Config(val dummy: Option[String], val tag: String)

class RelationsBot1(api: InstagramAPI, descr: BotDescriptor, config: RelationsBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var accountId: String = null
  
  var processedFormsIndex: Int = 0

  var processedFormsList: List[String] = null

  var processedUsersIndex: Int = 0

  var processedUsersList: List[(String, String)] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => commonCatched {
      accountId = api.getUserId
      if (accountId == null) finish else state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    }
    case InstagramCommonBotInfo.STATE_GET_TAG_FORMS => commonCatched {
      processedFormsIndex = 0
      processedFormsList = api.getRecentMediaTagForms(config.tag)
      if (processedFormsList.length > 0) state(InstagramCommonBotInfo.STATE_GET_USERS)
    }
    case InstagramCommonBotInfo.STATE_GET_USERS => if (processedFormsIndex < processedFormsList.length) commonCatched {
      processedUsersIndex = 0
      processedUsersList = api.getRecentUsersByTag(processedFormsList(processedFormsIndex))
      if (processedUsersList.length > 0) state(InstagramCommonBotInfo.STATE_SET_RELATIONS)
    }
    else state(InstagramCommonBotInfo.STATE_GET_TAG_FORMS)
    case InstagramCommonBotInfo.STATE_SET_RELATIONS =>
      if (processedUsersIndex < processedUsersList.length) commonChangeCatched(
        standartStatusPrepare(api.setSubscriberIfNot(accountId, processedUsersList(processedUsersIndex)._1)), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_USERS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    processedData += Map(
      "local_index" -> processedUsersIndex.toString,
      "user_id" -> processedUsersList(processedUsersIndex)._1,
      "user_name" -> processedUsersList(processedUsersIndex)._2,
      "status" -> status)
    processedUsersIndex += 1
  }

}

object RelationsBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState2(RelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.relationsBot1.create(uid, configForm)

  override def descriptionPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.relationsBot1.description(uid)

  def configForm: Form[RelationsBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "tag" -> nonEmptyText)(RelationsBot1Config.apply)(RelationsBot1Config.unapply))

  def name = "relationsbot1"

  def title = "Relations Bot 1 v0.1"

  def descr = "Подписывается на пользователей, владельцев недавних постов, найденных по формам указанного тэга."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new RelationsBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[RelationsBot1Config]))

}

