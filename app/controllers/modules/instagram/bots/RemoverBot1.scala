package controllers.modules.instagram.bots

import scala.collection.mutable.Buffer
import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses

case class RemoverBot1Config(val dummy: Option[String])

class RemoverBot1(api: InstagramAPI, descr: BotDescriptor, config: RemoverBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var userId: String = null

  var processedSubscribersIndex: Int = 0

  var processedSubscribersList: Buffer[(String, String)] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => commonCatched {
      userId = api.getUserId
      state(InstagramCommonBotInfo.STATE_GET_USERS)
    }
    case InstagramCommonBotInfo.STATE_GET_USERS => commonCatched {
      processedSubscribersIndex = 0
      processedSubscribersList = api.getTopSubscribersList(userId)
      if (processedSubscribersList.length > 0) state(InstagramCommonBotInfo.STATE_REMOVE_USERS) else finish
    }
    case InstagramCommonBotInfo.STATE_REMOVE_USERS =>
      if (processedSubscribersIndex < processedSubscribersList.length) commonChangeCatched(
        standartStatusPrepare(api.removeSubscriber(processedSubscribersList(processedSubscribersIndex)._1)), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_USERS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    processedData += Map(
      "local_index" -> processedSubscribersIndex.toString,
      "user_id" -> processedSubscribersList(processedSubscribersIndex)._1,
      "user_name" -> processedSubscribersList(processedSubscribersIndex)._2,
      "status" -> status)
    processedSubscribersIndex += 1
  }

}

object RemoverBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState2(RelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.removerBot1.create(uid, configForm)

  def configForm: Form[RemoverBot1Config] = Form(
    mapping(
      "dummy" -> optional(text))(RemoverBot1Config.apply)(RemoverBot1Config.unapply))

  def name = "removerbot1"

  def title = "Remover Bot 1 v0.1"

  def descr = "Удаляет всех подписчиков"

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new RemoverBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[RemoverBot1Config]))

}