package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses

case class TagsLikerBot1Config(val dummy: Option[String], val tags: String)

class TagsLikerBot1(api: InstagramAPI, descr: BotDescriptor, config: TagsLikerBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var processedTagsIndex: Int = 0

  var processedTagsList: List[String] = null

  var processedPostsIndex: Int = 0

  var processedPostsList: List[String] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => {
      processedTagsList = config.tags.split("\\s+").toList
      state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    }
    case InstagramCommonBotInfo.STATE_GET_RECENT_POSTS => if (processedTagsIndex < processedTagsList.length) commonCatched {
      processedPostsIndex = 0
      processedPostsList = api.getRecentMediaByTag(processedTagsList(processedTagsIndex))
      if (processedPostsList.length > 0) state(InstagramCommonBotInfo.STATE_LIKE_POSTS)
    }
    else processedTagsIndex = 0
    case InstagramCommonBotInfo.STATE_LIKE_POSTS =>
      if (processedPostsIndex < processedPostsList.length) commonChangeCatched(
        standartStatusPrepare(api.like(processedPostsList(processedPostsIndex))), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    processedData += Map(
      "local_index" -> processedPostsIndex.toString,
      "hash_tag" -> processedTagsList(processedTagsIndex),
      "media_id" -> processedPostsList(processedPostsIndex),
      "status" -> status)
    processedPostsIndex += 1
  }

}

object TagsLikerBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState1(RelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.tagsLikerBot1.create(uid, configForm)

  def configForm: Form[TagsLikerBot1Config] = Form(
    mapping(
      "dummy" -> optional(text),
      "tags" -> nonEmptyText)(TagsLikerBot1Config.apply)(TagsLikerBot1Config.unapply))

  def name = "tagslikerbot1"

  def title = "Tags Liker Bot 1 v0.1"

  def descr = "Лайкает недавние посты по указанному списку тэгов."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new TagsLikerBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[TagsLikerBot1Config]))

}

