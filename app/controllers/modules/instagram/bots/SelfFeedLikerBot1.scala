package controllers.modules.instagram.bots

import scala.collection.mutable.ListBuffer
import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import controllers.modules.bots.BotDescriptor
import controllers.modules.bots.BotSupporter
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import controllers.modules.instagram.InstagramAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.text
import controllers.modules.bots.CommonBotPauses

case class SelfFeedLikerBot1Config(val dummy: Option[String])

class SelfFeedLikerBot1(api: InstagramAPI, descr: BotDescriptor, config: SelfFeedLikerBot1Config) extends AbstractInstagramBotProcessor(api: InstagramAPI) {

  var userId: String = null

  var processedPostsIndex: Int = 0

  var processedPostsList: List[(String, String)] = null

  override def tick = state match {
    case CommonBotStates.STATE_INITIAL => commonCatched {
      userId = api.getUserId
      state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    }
    case InstagramCommonBotInfo.STATE_GET_RECENT_POSTS => commonCatched {
      processedPostsIndex = 0
      processedPostsList = api.getRecentSelfMediaFeed(userId)
      if (processedPostsList.length > 0) state(InstagramCommonBotInfo.STATE_LIKE_POSTS)
    }
    case InstagramCommonBotInfo.STATE_LIKE_POSTS =>
      if (processedPostsIndex < processedPostsList.length) commonChangeCatched(
        standartStatusPrepare(api.like(processedPostsList(processedPostsIndex)._1)), addProcessedData(InstagramCommonBotInfo.STATUS_ACCESS_DENIED))
      else state(InstagramCommonBotInfo.STATE_GET_RECENT_POSTS)
    case _ => super.tick
  }

  override def addProcessedData(status: String) {
    processedData += Map(
      "local_index" -> processedPostsIndex.toString,
      "media_id" -> processedPostsList(processedPostsIndex)._1,
      "user_name" -> processedPostsList(processedPostsIndex)._2,
      "status" -> status)
    processedPostsIndex += 1
  }

}

object SelfFeedLikerBot1Descr extends AbstractInstagramBotDescr {

  def resultPage(result: Any)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonState3(RelationsBot1Descr, result)

  def createPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.selfFeedLikerBot1.create(uid, configForm)

  def configForm: Form[SelfFeedLikerBot1Config] = Form(
    mapping(
      "dummy" -> optional(text))(SelfFeedLikerBot1Config.apply)(SelfFeedLikerBot1Config.unapply))

  def name = "selffeedlikerbot1"

  def title = "Self Feed Liker Bot 1 v0.1"

  def descr = "Лайкает недавние посты среди своих подписок."

  def create(api: InvolvedAPI, config: Any): BotSupporter = new BotSupporter(this, new SelfFeedLikerBot1(api.asInstanceOf[InstagramAPI], this, config.asInstanceOf[SelfFeedLikerBot1Config]))

}

