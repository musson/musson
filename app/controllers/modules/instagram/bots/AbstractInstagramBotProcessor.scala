package controllers.modules.instagram.bots

import org.jinstagram.exceptions.InstagramBadRequestException
import org.jinstagram.exceptions.InstagramException
import org.jinstagram.exceptions.InstagramRateLimitException
import controllers.modules.bots.InvolvedAPI
import controllers.modules.bots.StateBotProcessor
import play.api.Logger
import controllers.modules.bots.CommonBotStates
import controllers.modules.bots.BotDescriptor
import scala.collection.mutable.ListBuffer

abstract class AbstractInstagramBotProcessor(api: InvolvedAPI) extends StateBotProcessor(api: InvolvedAPI, InstagramCommonBotInfo.CATEGORY_ISNTAGRAM) {

  val processedData: ListBuffer[Map[String, String]] = new ListBuffer[Map[String, String]]

  def commonCatched(f: => Any) = try (f) catch {
    case e: InstagramBadRequestException => if (e.getMessage.contains(InstagramCommonBotInfo.ER_MSG_3)) pushAndState(InstagramCommonBotInfo.STATE_ACCESS_TOKEN_INVALID, e) else pushAndFinishOnError(e)
    case e: InstagramException           => if (InstagramCommonBotInfo.IGNORED_ERR_WITH_CONTINUE exists { value => e.getMessage.contains(value) }) pushAndState(InstagramCommonBotInfo.STATE_IGNORED_WITH_CONTINUE_ERR, e) else pushAndFinishOnError(e)
    case e: Exception                    => if (InstagramCommonBotInfo.APP_ERR_IGNORED exists { value => e.getMessage.contains(value) }) pushAndState(InstagramCommonBotInfo.STATE_APP_ERR_IGNORED, e) else pushAndFinishOnError(e)
    case e: Throwable                    => pushAndFinishOnError(e)
  }

  def commonChangeCatched(f: => Any, accessDeniedF: => Any) = try (f) catch {
    case e: InstagramBadRequestException =>
      if (InstagramCommonBotInfo.ACCESS_DENIED_ERR exists { value => e.getMessage.contains(value) }) accessDeniedF
      else if (e.getMessage.contains(InstagramCommonBotInfo.ER_MSG_3)) pushAndState(InstagramCommonBotInfo.STATE_ACCESS_TOKEN_INVALID, e)
      else pushAndFinishOnError(e)
    case e: InstagramException =>
      if (e.getMessage.contains(InstagramCommonBotInfo.ER_MSG_2)) pushAndState(CommonBotStates.STATE_TIME_LIMIT, e)
      else if (InstagramCommonBotInfo.IGNORED_ERR_WITH_CONTINUE exists { value => e.getMessage.contains(value) }) pushAndState(InstagramCommonBotInfo.STATE_IGNORED_WITH_CONTINUE_ERR, e)
      else pushAndFinishOnError(e)
    case e: Exception => if (InstagramCommonBotInfo.APP_ERR_IGNORED exists { value => e.getMessage.contains(value) }) pushAndState(InstagramCommonBotInfo.STATE_APP_ERR_IGNORED, e) else pushAndFinishOnError(e)
    case e: Throwable => pushAndFinishOnError(e)
  }

  override def logError(e: Throwable) = e match {
    case ex: InstagramRateLimitException  => Logger.error("Instagram rate limit exception: ", ex)
    case ex: InstagramBadRequestException => Logger.error("Instagram bad request exception: ", ex)
    case ex: InstagramException           => Logger.error("Instagram exception: ", ex)
    case ex: Throwable                    => super.logError(e)
  }

  override def tick = state match {
    case InstagramCommonBotInfo.STATE_ACCESS_TOKEN_INVALID => {}
    case InstagramCommonBotInfo.STATE_IGNORED_WITH_CONTINUE_ERR | InstagramCommonBotInfo.STATE_APP_ERR_IGNORED => back
    case _ => super.tick
  }

  override def selectPause: Long = state match {
    case InstagramCommonBotInfo.STATE_GET_TAG_FORMS => InstagramCommonBotInfo.TIME_READ_PAUSE
    case InstagramCommonBotInfo.STATE_GET_RECENT_POSTS => InstagramCommonBotInfo.TIME_READ_PAUSE
    case InstagramCommonBotInfo.STATE_GET_USERS => InstagramCommonBotInfo.TIME_READ_PAUSE
    case InstagramCommonBotInfo.STATE_LIKE_POSTS => InstagramCommonBotInfo.TIME_CHANGE_LIKES_PAUSE
    case InstagramCommonBotInfo.STATE_SET_RELATIONS => InstagramCommonBotInfo.TIME_CHANGE_RELATIONS_PAUSE
    case InstagramCommonBotInfo.STATE_REMOVE_USERS => InstagramCommonBotInfo.TIME_CHANGE_RELATIONS_PAUSE
    case InstagramCommonBotInfo.STATE_IGNORED_WITH_CONTINUE_ERR => InstagramCommonBotInfo.timeLimitValueIgnored
    case InstagramCommonBotInfo.STATE_ACCESS_TOKEN_INVALID => InstagramCommonBotInfo.timeLimitValueInvalidAccessToken
    case _ => super.selectPause
  }

  def standartStatusPrepare(f: => String) = f match {
    case InstagramCommonBotInfo.STATUS_SUCCESS => addProcessedData(InstagramCommonBotInfo.STATUS_SUCCESS)
    case InstagramCommonBotInfo.STATUS_BEFORE => {
      addProcessedData(InstagramCommonBotInfo.STATUS_BEFORE)
      micropause
    }
  }

  def addProcessedData(status: String) = {}

  def getCommonStatsInfo = Map[String, Int](
    InstagramCommonBotInfo.ALL -> processedData.length,
    InstagramCommonBotInfo.STATUS_SUCCESS -> processedData.filter(p => p(InstagramCommonBotInfo.STATUS).equals(InstagramCommonBotInfo.STATUS_SUCCESS)).length,
    InstagramCommonBotInfo.STATUS_BAD_REQUEST -> processedData.filter(p => p(InstagramCommonBotInfo.STATUS).equals(InstagramCommonBotInfo.STATUS_BAD_REQUEST)).length,
    InstagramCommonBotInfo.STATUS_BEFORE -> processedData.filter(p => p(InstagramCommonBotInfo.STATUS).equals(InstagramCommonBotInfo.STATUS_BEFORE)).length,
    InstagramCommonBotInfo.STATUS_ACCESS_DENIED -> processedData.filter(p => p(InstagramCommonBotInfo.STATUS).equals(InstagramCommonBotInfo.STATUS_ACCESS_DENIED)).length);

  def prepareResultWithStats: Map[String, Any] = prepareResult + (InstagramCommonBotInfo.COMMON_STATS_INFO -> getCommonStatsInfo)

  override def prepareResult: Map[String, Any] = super.prepareResult + (CommonBotStates.RESULT -> processedData.clone(), CommonBotStates.INFO -> infoStack)

  override def immutableResult: Any = prepareResultWithStats

}

abstract class AbstractInstagramBotDescr extends BotDescriptor {

  def descriptionPage(uid: Long)(implicit flash: play.api.mvc.Flash, lang: play.api.i18n.Lang, request: play.api.mvc.RequestHeader, user: models.User) =
    views.html.modules.instagram.bots.commonDescription(uid, this)

}
