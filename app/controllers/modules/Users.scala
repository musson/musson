package controllers.modules

import controllers.AppImplicits.appContext
import controllers.AuthConfigImpl
import controllers.Application
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import models.User
import play.api.i18n.Messages
import play.api.mvc.Controller
import views.html
import controllers.Application
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.Logger

object Users extends Controller with AsyncAuth with AuthConfigImpl {

  val newUserForm = Form {
    mapping(
      "name" -> optional(nonEmptyText),
      "surname" -> optional(nonEmptyText),
      "email" -> email.verifying("Email already exists", User.findByEmail(_).isEmpty),
      "password" -> nonEmptyText.verifying("Password sould contains english symbols and numbers, and have size more than 7 characters!", {
        _ match {
          case value: String => value.size > 7 && value.forall(Application.ordinary.contains(_))
          case _             => false
        }
      }))(User.newUser)(_.map(u => (u.name, u.surname, u.email, "")))
      .verifying("Can't create new user", result => result.isDefined)
  }

  def show = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Redirect(controllers.modules.routes.Users.list)
  }

  def list = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.users.list())
  }

  def create = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.users.create(newUserForm))
  }

  def createProcess = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      newUserForm.bindFromRequest.fold(
        formWithErrors => BadRequest(html.modules.users.create(formWithErrors)),
        userInCreate =>
          User.create(userInCreate.get) match {
            case Some(createdUser) => {
              Logger.debug("User created: " + userInCreate.get.email)
              Redirect(controllers.modules.routes.Users.list).flashing("success" -> Messages("module.users.create.success", "\"" + user.email + "\""))
            }
            case _ => {
              Logger.error("Can't create user: " + userInCreate.get.email)
              BadRequest
            }
          })
  }

  def remove(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      User.delete(id) match {
        case 1L => Redirect(controllers.modules.routes.Users.list).
          flashing("success" -> Messages("module.users.remove.success", "\"" + user.id + "\""))
        case _ => BadRequest
      }
  }

  def page(pageNumber: Int, filter: String) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      Ok(html.modules.users.filteredPage(User.page(pageNumber, Application.assemblyFilter(filter): _*)))
  }

  def profile(id: Long) = authorizedAction(Permission.administartor) { implicit user =>
    implicit request =>
      User.findById(id).map(viewedUser => Ok(html.modules.users.profile(viewedUser))).getOrElse(BadRequest)
  }

}
