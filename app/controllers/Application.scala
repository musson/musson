package controllers

import scala.concurrent.Future
import jp.t2v.lab.play2.auth.LoginLogout
import models.User
import models.Content
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Action
import play.api.mvc.Controller
import java.nio.file.Path
import java.nio.file.Paths
import jp.t2v.lab.play2.auth.AsyncAuth
import models.Permission
import play.Logger
import views.html.defaultpages.unauthorized
import views.html
import jp.t2v.lab.play2.auth.AuthenticationElement
import jp.t2v.lab.play2.auth.OptionalAuthElement
import play.api.data.validation.Constraint
import play.api.data.validation.Valid
import play.api.data.validation.Invalid
import models.ActivationCode
import play.api.Play.current
import play.api.libs.mailer._
import controllers.modules.instagram.InstagramModule
import controllers.modules.instagram.InstagramParamsInitializer
import play.api.mvc.AnyContent
import play.api.mvc.Result
import controllers.modules.BaseSettings

object Application extends Controller with AsyncAuth with LoginLogout with AuthConfigImpl {

  val PAGE_SIZE = 10

  val RETURN_URL = "returnURL"

  val ordinary = (('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')).toSet

  val loginForm = Form {
    mapping(
      "email" -> email,
      "password" -> text)(User.authenticate)(_.map(u => (u.email, "")))
      .verifying("Invalid email or password", result => result.isDefined)
  }

  def passwordValidator(value: String) = Constraint[String](value) {
    case "once" => Valid
    case _      => Invalid("Must be a negative number.")
  }

  val registerForm = Form {
    mapping(
      "email" -> email.verifying("Email already exists", User.findByEmail(_).isEmpty),
      "password" -> nonEmptyText.verifying("Password sould contains english symbols and numbers, and have size more than 7 characters!", {
        _ match {
          case value: String => value.size > 7 && value.forall(ordinary.contains(_))
          case _             => false
        }
      }))(User.tryRegister)(_.map(u => (u.email, "")))
      .verifying("Can't register new user", result => result.isDefined)
  }

  def index = Pages.showOrNotFoundServicePage(Content.ROLE_INDEX)

  // replace with unauthorized action
  def register = Action { implicit request =>
    if (BaseSettings.isActiveRegister) {
      implicit val user = User.guest
      Dynamic.render("register", registerForm, request2flash, request2lang, user) match {
        case Some(page) => Ok(page)
        case None       => Ok(html.adminRegister(registerForm))
      }
    } else NotFound
  }

  // replace with unauthorized action
  def login = Action { implicit request =>
    Logger.debug("login()")
    implicit val user = User.guest
    Dynamic.render("login", loginForm, request2flash, request2lang, user) match {
      case Some(page) => Ok(page)
      case None       => Ok(html.adminLogin(loginForm))
    }
  }

  // replace with any authorized user action
  def logout = authorizedAction.async(Permission.normalUser) { implicit user =>
    implicit request =>
      Logger.debug("logout()")
      if (user.offline == 0) Logger.error("Can't set offline status for user: " + user.id)
      gotoLogoutSucceeded
  }

  def confirmRegister(code: String) = Action { implicit request =>
    if (BaseSettings.isActiveRegister) {
      ActivationCode.findByCode(code) match {
        case Some(activationCode) => {
          if (activationCode.isExpired) {
            Logger.error("Activation code: " + code + "  - already expired!")
            BadRequest
          } else {
            User.findById(activationCode.ownerId) match {
              case Some(activatedUser) => {
                activatedUser.accountStatus = User.ACCOUNT_STATUS_ENABLED
                User.update(activatedUser) match {
                  case 1 => {
                    ActivationCode.delete(activationCode.id) match {
                      case 1 => Logger.debug("Successfully deleted activation code: " + activationCode.id)
                      case _ => Logger.error("Can't delete activation code: " + activationCode.id)
                    }
                    Dynamic.render("successConfirmRegister", activatedUser, request2flash, request2lang, User.guest) match {
                      case Some(page) => Ok(page)
                      case None => {
                        implicit val user = User.guest
                        Ok(html.successConfirmRegister(activatedUser))
                      }
                    }
                  }
                  case _ => {
                    Logger.error("Can't update activation user: " + activatedUser.id)
                    BadRequest
                  }
                }
              }
              case _ => {
                Logger.error("Can't find activation user: " + activationCode.ownerId)
                BadRequest
              }
            }
          }
        }
        case _ => {
          Logger.error("Can't find activation code in db: " + code)
          BadRequest // TODO: report about error
        }
      }
    } else NotFound
  }

  // replace with unathorized action
  def registerProcess = Action { implicit request =>
    if (BaseSettings.isActiveRegister) {
      registerForm.bindFromRequest.fold(
        formWithErrors =>
          Dynamic.render("register", formWithErrors, request2flash, request2lang, User.guest) match {
            case Some(page) => BadRequest(page)
            case None => {
              implicit val user = User.guest
              BadRequest(html.adminRegister(formWithErrors))
            }
          }, {
          userInRegister =>
            User.create(userInRegister.get) match {
              case Some(createdUser) => {
                ActivationCode.createRegisterActionCode(createdUser) match {
                  case Some(activationCode) => {
                    try {
                      MailerPlugin.send(Email(
                        "Your access code to Musson",
                        "Musson support<support@musson.com>",
                        Seq(createdUser.email),
                        bodyHtml = Some("<html><body><p>To confirm your account click <a href=\"http://" + InstagramParamsInitializer.appAdress + "/confirmRegister/" + activationCode.code + "\">this<a/>.</p></body></html>")))
                      Dynamic.render("afterRegister", activationCode, request2flash, request2lang, createdUser) match {
                        case Some(page) => Ok(page)
                        case None => {
                          implicit val user = createdUser
                          Ok(html.adminAfterRegister(activationCode))
                        }
                      }
                    } catch {
                      case e: Throwable => if (e.getMessage.contains("EmailException")) {
                        Logger.error("Email sending error:", e)
                        BadRequest
                      } else {
                        Logger.error("Exception:", e)
                        BadRequest
                      }
                    }
                  }
                  case _ => {
                    Logger.error("Can't create activation code: " + createdUser.id)
                    Logger.error("Try to remove user from db...")
                    User.delete(createdUser.id) match {
                      case 1 => Logger.error("User removed successfuly: " + createdUser.id)
                      case _ => Logger.error("Can't remove user from db: " + +createdUser.id)
                    }
                    BadRequest
                  }
                }
              }
              case _ => {
                Logger.error("Can't create user after registration process: " + userInRegister.get.email)
                BadRequest
              }
            }
        })
    } else NotFound
  }

  // replace with unathorized action
  def authenticate = Action.async { implicit request =>
    Logger.debug("authenticate()")
    loginForm.bindFromRequest.fold(
      formWithErrors => Future.successful(
        Dynamic.render("login", formWithErrors, request2flash, request2lang, User.guest) match {
          case Some(page) => BadRequest(page)
          case None => {
            implicit val user = User.guest
            BadRequest(html.adminLogin(formWithErrors))
          }
        }), {
        user =>
          user.get.online match {
            case 1 => gotoLoginSucceeded(user.get.id)
            case _ => {
              Logger.error("Can't set online status for user: " + user.get.id)
              Future.successful(BadRequest)
            }
          }
      })
  }

  def assemblyFilter(filterStr: String, defaultFilter: (String, String)*) = {
    val splitted = filterStr.split("/")
    if (splitted.length % 2 == 0) splitted.grouped(2).map { case Array(a, b) => (a, b) }.toList ++ defaultFilter else defaultFilter
  }

}
