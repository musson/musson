package controllers

import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.Content
import models.User
import play.api.i18n.Lang
import play.api.mvc.Controller
import play.api.mvc.Flash
import play.twirl.api.Html

object Pages extends Controller with OptionalAuthElement with AuthConfigImpl {
  
  def showOrNotFoundServicePage(role: String) = StackAction { implicit request =>
    Content.findUnique("role" -> role).map { page =>
      Dynamic.render("postPage", page, request2flash, request2lang, loggedIn.getOrElse(User.guest)) match {
        case Some(page) => Ok(page)
        case None       => NotFound
      }
    }.getOrElse(NotFound)
  }

  def show(id: Long) = StackAction { implicit request =>
    Content.findById(id).map { page =>
      Dynamic.render("postPage", page, request2flash, request2lang, loggedIn.getOrElse(User.guest)) match {
        case Some(page) => Ok(page)
        case None       => NotFound
      }
    }.getOrElse(NotFound)
  }

  def showWrappedPostContent(pageId: String, title: String, content: Html, user: User, request2flash: Flash, request2lang: Lang) = {
    Dynamic.render("postContentPage", pageId, title, content, request2flash, request2lang, user) match {
      case Some(page) => Ok(page)
      case None       => NotFound
    }
  }

}
