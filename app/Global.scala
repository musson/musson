import java.io.File
import com.typesafe.config.ConfigFactory
import play.api.Application
import play.api.Configuration
import play.api.GlobalSettings
import play.api.Logger
import play.api.Mode
import play.api.mvc.RequestHeader
import models.User
import scala.concurrent.Future
import play.api.mvc.Results.InternalServerError
import play.api.mvc.Results.NotFound
import play.api.mvc.Results.BadRequest
import play.api.i18n.Lang
import play.api.mvc.Flash

object Global extends GlobalSettings {

  override def onStart(app: Application) = InitialData.insert

  override def onLoadConfig(config: Configuration, path: File, classloader: ClassLoader, mode: Mode.Mode): Configuration = {
    val systemUserName = System.getProperty("user.name")
    val configFileName = s"application.${systemUserName}.conf"
    Logger.info("Load configuration from: " + configFileName)
    val environmentSpecificConfig = config ++ Configuration(ConfigFactory.load(configFileName))
    super.onLoadConfig(environmentSpecificConfig, path, classloader, mode)
  }

  override def onError(request: RequestHeader, ex: Throwable) = {
    Future.successful(InternalServerError(
      views.html.errorPage( /*ex.toString()*/ "Some problems.")))
  }

  override def onHandlerNotFound(request: RequestHeader) = {
    Future.successful(NotFound(
      views.html.errorPage("Not found: " + request.path)))
  }

  override def onBadRequest(request: RequestHeader, error: String) = {
    Future.successful(BadRequest(views.html.errorPage("Bad Request: " + error)))
  }

}