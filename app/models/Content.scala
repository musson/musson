package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import controllers.Application
import play.api.Play.current
import play.api.db.DB
import java.util.Date
import controllers.Datetime
import play.Logger

case class Content(var id: Long,
                   var ownerId: Long,
                   var parentId: Option[Long],
                   var title: Option[String],
                   var data: Option[String],
                   var status: Option[String],
                   var kind: String,
                   var role: String,
                   mimeType: Option[String],
                   created: Date,
                   modified: Option[Date])

object Content extends CRUDModel[Content] {

  table = "content"

  val ALL = "all"

  val STATE_DRAFT = "draft"

  val STATE_PUBLISHED = "published"

  val KIND_PAGE = "page"
  
  val KIND_INCLUDE_HEADER = "includeHeader"
  
  val KIND_INCLUDE_BODY = "includeBody"
  
  val KIND_INCLUDE_FOOTER = "includeFooter"

  val KIND_ATTACHMENT = "attachment";

  val ROLE_INDEX = "index"

  val ROLE_NO = "no"

  val KIND_POST = "post";

  def apply(id: Long,
            ownerId: Long,
            parentId: Option[Long],
            title: String,
            data: Option[String],
            status: Option[String],
            kind: String,
            role: String,
            mimeType: Option[String],
            created: Date,
            modified: Option[Date]): Content = {
    Content(id, ownerId, parentId, Some(title), data, status, kind, role, mimeType, created, modified)
  }

  def extract(content: Content) = Some(content.id,
    content.ownerId,
    content.parentId,
    content.title.get,
    content.data,
    content.status,
    content.kind,
    content.role,
    content.mimeType,
    content.created,
    content.modified)

  def apply(ownerId: Long, parentId: Option[Long], title: Option[String], data: Option[String], status: Option[String], kind: String, role: String, mimeType: Option[String]): Content =
    Content(-1L, ownerId, parentId, title, data, status, kind, role, mimeType, Datetime.dtCurrentUTC, None)

  def apply(ownerId: Long, title: Option[String], data: Option[String], status: Option[String], kind: String, role: String, mimeType: Option[String]): Content =
    Content(-1L, ownerId, None, title, data, status, kind, role, mimeType, Datetime.dtCurrentUTC, None)

  parser = get[BigInteger]("id") ~
    get[BigInteger]("owner_id") ~
    get[Option[BigInteger]]("parent_id") ~
    get[Option[String]]("title") ~
    get[Option[String]]("data") ~
    get[Option[String]]("status") ~
    get[String]("kind") ~
    get[String]("role") ~
    get[Option[String]]("mime_type") ~
    get[Date]("created") ~
    get[Option[Date]]("modified") map {
      case id ~ owner_id ~ parent_id ~ title ~ data ~ status ~ kind ~ role ~ mimeType ~ created ~ modified =>
        Content(
          id.longValue,
          owner_id.longValue,
          parent_id.map { pid => Some(pid.longValue) }.getOrElse(None),
          title,
          data,
          status,
          kind,
          role,
          mimeType,
          created,
          modified)
    }

  def create(content: Content): Option[Content] = DB.withConnection {
    implicit connection =>
      SQL("""
          INSERT INTO content(owner_id, parent_id, title, data, status, kind, role, mime_type, created, modified) 
          VALUES ({ownerId}, {parentId}, {title}, {data}, {status}, {kind}, {role}, {mimeType}, {created}, {modified})
        """).on(
        'title -> content.title,
        'ownerId -> content.ownerId,
        'parentId -> content.parentId,
        'data -> content.data,
        'status -> content.status,
        'kind -> content.kind,
        'role -> content.role,
        'mimeType -> content.mimeType,
        'created -> content.created,
        'modified -> content.modified).executeInsert() match {
          case id: Some[Long] => {
            Some(Content(
              id.get,
              content.ownerId,
              content.parentId,
              content.title,
              content.data,
              content.status,
              content.kind,
              content.role,
              content.mimeType,
              content.created,
              content.modified))
          }
          case None => None
        }
  }
  
  def update(content: Content): Long = DB.withConnection {
    implicit connection =>
      SQL("""
            UPDATE content 
            SET owner_id = {ownerId}, parent_id = {parentId}, title = {title}, data = {data}, status = {status}, kind = {kind}, role = {role}, mime_type = {mimeType}, created = {created}, modified = {modified}      
            WHERE id = {id}
        """).on(
        "id" -> content.id,
        "ownerId" -> content.ownerId,
        "parentId" -> content.parentId,
        "title" -> content.title,
        "data" -> content.data,
        "status" -> content.status,
        "kind" -> content.kind,
        "role" -> content.role,
        "mimeType" -> content.mimeType,
        "created" -> Datetime.dtsFromUTCDate("UTC", content.created),
        "modified" -> Datetime.dtsCurrentUTC).executeUpdate()
  }
  
  def findPublishedPages = DB.withConnection {
    implicit connection =>
      SQL("SELECT * FROM " + table + " WHERE kind = {kind} AND status = {status} ORDER BY id ASC").on(
        "kind" -> KIND_PAGE,
        "status" -> STATE_PUBLISHED).as(parser *)
  }

}
