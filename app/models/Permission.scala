package models

import java.math.BigInteger

import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB

case class Permission(id: Long, name: String)

object Permission extends CRUDModel[Permission] {

  table = "permissions"

  val PERMISSION_ADMINISTRATOR = "admin"

  val PERMISSION_NORMAL_USER = "normal_user"

  val PERMISSION_GUEST_USER = "guest_user"

  def apply(name: String): Permission = Permission(-1L, name)

  parser = get[BigInteger]("id") ~
    get[String]("name") map {
      case id ~ name => Permission(id.longValue, name)
    }

  def create(permission: Permission): Option[Permission] = DB.withConnection {
    implicit connection =>
      SQL("insert into " + table + "(name) values ({name})").on(
        'name -> permission.name).executeInsert() match {
          case id: Some[Long] => Some(Permission(id.get, permission.name))
          case None           => None
        }
  }

  def findByName(name: String): Option[Permission] = findUnique("name" -> name)

  def administartor = findByName(Permission.PERMISSION_ADMINISTRATOR).get

  def normalUser = findByName(Permission.PERMISSION_NORMAL_USER).get

  def guestUser = findByName(Permission.PERMISSION_GUEST_USER).get

}
