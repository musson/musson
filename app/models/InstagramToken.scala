package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import controllers.Application
import play.api.Play.current
import play.api.db.DB
import java.util.Date
import controllers.Datetime
import play.Logger

case class InstagramToken(var id: Long, var userId: Long, var tokenId: Int, code: Option[String], token: Option[String], secret: Option[String], raw: Option[String])

object InstagramToken extends CRUDModel[InstagramToken] {

  table = "instagram_tokens"

  def apply(userId: Long, tokenId: Int, code: Option[String], token: Option[String], secret: Option[String], raw: Option[String]): InstagramToken =
    InstagramToken(-1L, userId, tokenId, code, token, secret, raw)

  parser = get[BigInteger]("id") ~
    get[BigInteger]("user_id") ~
    get[Int]("token_id") ~
    get[Option[String]]("code") ~
    get[Option[String]]("token") ~
    get[Option[String]]("secret") ~
    get[Option[String]]("raw") map {
      case id ~ user_id ~ token_id ~ code ~ token ~ secret ~ raw => InstagramToken(id.longValue, user_id.longValue, token_id, code, token, secret, raw)
    }

  def create(token: InstagramToken): Option[InstagramToken] = DB.withConnection {
    implicit connection =>
      SQL("""
          INSERT INTO instagram_tokens(user_id, token_id, code, token, secret, raw) VALUES ({userId}, {tokenId}, {code}, {token}, {secret}, {raw})
        """).on(
        'userId -> token.userId,
        'tokenId -> token.tokenId,
        'code -> token.code,
        'token -> token.token,
        'secret -> token.secret,
        'raw -> token.raw).executeInsert() match {
          case id: Some[Long] => {
            Some(InstagramToken(
              id.get,
              token.userId,
              token.tokenId,
              token.code,
              token.token,
              token.secret,
              token.raw))
          }
          case None => None
        }
  }

  def update(token: InstagramToken): Long = DB.withConnection {
    implicit connection =>
      SQL("""
            UPDATE instagram_tokens SET user_id = {userId}, token_id={tokenId}, code = {code}, token = {token}, secret = {secret}, raw = {raw} WHERE id = {id}
        """).on(
        "id" -> token.id,
        "user_id" -> token.userId,
        "token_id" -> token.tokenId,
        "code" -> token.code,
        "token" -> token.token,
        "secret" -> token.secret,
        "raw" -> token.raw).executeUpdate()
  }

  def findToken(uid: Long, tokenId: Int): Option[InstagramToken] = findUnique("user_id" -> uid.toString, "token_id" -> tokenId.toString)

}

