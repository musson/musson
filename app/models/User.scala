package models

import java.math.BigInteger
import org.mindrot.jbcrypt.BCrypt
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import controllers.Application
import play.api.Play.current
import play.api.db.DB
import java.util.Date
import java.util.TimeZone
import controllers.Datetime

case class User(id: Long, email: String, hash: Option[String], permission: Permission, avatarId: Option[Long],
                var userStatus: String, var accountStatus: String, name: Option[String], surname: Option[String], timezone: String, registered: Date,
                phone: Option[String], userType: String) {

  def online = {
    userStatus = User.USER_STATUS_ONLINE
    User.update(this)
  }

  def offline = {
    userStatus = User.USER_STATUS_ONLINE
    User.update(this)
  }

  def getAvatarLink = avatarId.map { aid => controllers.routes.Assets.at(controllers.Media.getAttachmentLink(aid)) }.getOrElse(
    controllers.routes.Assets.at(controllers.Media.getAttachmentLink(3)))

}

object User extends CRUDModel[User] {

  table = "users"

  val STATUS_ALL = "all"

  val USER_TYPE_SUBSCRIBER = "subscriber"

  val USER_TYPE_CLIENT = "client"

  val USER_TYPE_NORMAL = "normal"

  val USER_STATUS_ONLINE = "online"

  val USER_STATUS_OFFLINE = "offline"

  val ACCOUNT_STATUS_WAIT_FOR_ACTIVATION = "wait_for_activation"

  val ACCOUNT_STATUS_ENABLED = "enabled"

  val ACCOUNT_STATUS_DISABLED = "disabled"

  val ACCOUNT_STATUS_GUEST = "guest"

  def guest = User(
    null,
    null,
    Permission.guestUser,
    Some(3),
    User.USER_STATUS_ONLINE,
    User.ACCOUNT_STATUS_GUEST, None, None, Datetime.timezone, None, USER_TYPE_NORMAL)

  def apply(email: String, hash: Option[String], permission: Permission, avatarId: Option[Long],
            userStatus: String, accountStatus: String, name: Option[String], surname: Option[String], timezone: String, phone: Option[String], userType: String): User =
    User(-1L, email, hash, permission, avatarId, userStatus, accountStatus, name, surname, timezone, Datetime.dtCurrentUTC, phone,
      userType)

  parser = get[BigInteger]("id") ~
    get[String]("email") ~
    get[Option[String]]("hash") ~
    get[BigInteger]("permission_id") ~
    get[Option[BigInteger]]("avatar_id") ~
    get[String]("user_status") ~
    get[String]("account_status") ~
    get[Option[String]]("name") ~
    get[Option[String]]("surname") ~
    get[String]("timezone") ~
    get[Date]("registered") ~
    get[Option[String]]("phone") ~
    get[String]("user_type") map {
      case id ~ email ~ hash ~ permission_id ~ avatar_id ~ user_status ~ account_status ~ name ~ surname ~ timezone ~ registered ~ phone ~ user_type =>
        User(id.longValue, email, hash, Permission.findById(permission_id.longValue).get,
          avatar_id.map(aid => Some(aid.longValue)).getOrElse(None),
          user_status, account_status, name, surname,
          timezone, registered, phone, user_type)
    }

  def create(user: User): Option[User] = DB.withConnection {
    implicit connection =>
      SQL("""
          INSERT INTO users(email, hash, permission_id, avatar_id, user_status, account_status, name, surname, timezone, registered, phone, user_type) 
          VALUES ({email}, {hash}, {permissionId}, {avatarId}, {userStatus}, {accountStatus}, {name}, {surname}, {timezone}, {registered},
            {phone}, {userType})
        """).on(
        'email -> user.email,
        'hash -> user.hash,
        'permissionId -> user.permission.id,
        'avatarId -> user.avatarId,
        'userStatus -> user.userStatus,
        'accountStatus -> user.accountStatus,
        'name -> user.name,
        'surname -> user.surname,
        'timezone -> user.timezone,
        'registered -> Datetime.dtsCurrentUTC,
        'phone -> user.phone,
        'userType -> user.userType).executeInsert() match {
          case id: Some[Long] => Some(
            User(
              id.get,
              user.email,
              user.hash,
              user.permission,
              user.avatarId,
              user.userStatus,
              user.accountStatus,
              user.name,
              user.surname,
              user.timezone,
              user.registered,
              user.phone,
              user.userType))
          case None => None
        }
  }

  def update(user: User): Long = DB.withConnection {
    implicit connection =>
      SQL("""
            UPDATE users 
            SET id = {id}, email = {email}, hash = {hash}, permission_id = {permission}, avatar_id = {avatarId}, 
               user_status = {userStatus}, account_status = {accountStatus}, name = {name}, surname = {surname},
               timezone = {timezone}, registered = {registered}, phone = {phone}, user_type = {userType}  
            WHERE id = {id}
        """).on(
        "id" -> user.id,
        "email" -> user.email,
        "hash" -> user.hash,
        "permission" -> user.permission.id,
        "avatarId" -> user.avatarId,
        "userStatus" -> user.userStatus,
        "accountStatus" -> user.accountStatus,
        "name" -> user.name,
        "surname" -> user.surname,
        "timezone" -> user.timezone,
        "registered" -> user.registered,
        "phone" -> user.phone,
        "userType" -> user.userType).executeUpdate()
  }

  def findByEmail(email: String): Option[User] = findUnique("email" -> email)

  def findByEmailEnabled(email: String): Option[User] = findUnique("email" -> email, "account_status" -> ACCOUNT_STATUS_ENABLED)

  def tryRegister(email: String, password: String): Option[User] = Some(User(
    email,
    Some(BCrypt.hashpw(password, BCrypt.gensalt())),
    Permission.normalUser,
    Some(1),
    User.USER_STATUS_OFFLINE,
    User.ACCOUNT_STATUS_DISABLED, None, None, Datetime.timezone, None, USER_TYPE_NORMAL))

  def newUser(name: Option[String], surname: Option[String], email: String, password: String): Option[User] = Some(User(
    email,
    Some(BCrypt.hashpw(password, BCrypt.gensalt())),
    Permission.normalUser,
    Some(1),
    User.USER_STATUS_OFFLINE,
    User.ACCOUNT_STATUS_ENABLED, name, surname, Datetime.timezone, None, USER_TYPE_NORMAL))

  def authenticate(email: String, password: String): Option[User] = findByEmailEnabled(email).filter { user =>
    user.hash.map { hash => BCrypt.checkpw(password, hash) }.getOrElse(false)
  }

}
