package models

import anorm.NamedParameter.string
import anorm.RowParser
import anorm.SQL
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import controllers.Application

trait CRUDModel[T] {

  var table: String = _

  var parser: RowParser[T] = _

  def count = DB.withConnection {
    implicit connection =>
      SQL("SELECT COUNT(*) AS c FROM " + table).apply().head[Long]("c")
  }

  def delete(id: Long): Long = DB.withConnection {
    implicit connection =>
      SQL("DELETE FROM " + table + " WHERE id = {id}").on("id" -> id).executeUpdate()
  }

  def findById(id: Long): Option[T] = DB.withConnection {
    implicit connection =>
      SQL("SELECT * FROM " + table + " WHERE id = {id}").on("id" -> id).as(parser.singleOpt)
  }

  def findUnique(filter: (String, String)*): Option[T] = DB.withConnection {
    implicit connection =>
      SQL("SELECT * FROM " + table + (for ((key, value) <- filter) yield key + " = " + "{" + key + "}").mkString(" WHERE ", " AND ", ""))
        .onParams(filter.map(entry => entry._2: anorm.ParameterValue).toSeq: _*).as(parser.singleOpt)
  }

  def findLastLimited(limit: Int, filter: (String, String)*): List[T] = DB.withConnection {
    implicit connection =>
      SQL("SELECT * FROM " + table + (for ((key, value) <- filter) yield key + " = " + "{" + key + "}").mkString(" WHERE ", " AND ", "") +
        "ORDER BY id DESC LIMIT {limit}")
        .onParams(filter.map(entry => entry._2: anorm.ParameterValue).toSeq ++ Seq(limit: anorm.ParameterValue): _*).as(parser *)
  }

  def count(filter: (String, String)*): Long = DB.withConnection {
    implicit connection =>
      SQL("SELECT COUNT(*) AS c FROM " + table +
        (if (filter.isEmpty) "" else (for ((key, value) <- filter) yield key + " = " + "{" + key + "}").mkString(" WHERE ", " AND ", "")))
        .onParams(filter.map(entry => entry._2: anorm.ParameterValue).toSeq: _*).apply()
        .head[Long]("c")
  }

  def pagesCount(filter: (String, String)*) = {
    val rowCount = count(filter: _*)
    if (rowCount % Application.PAGE_SIZE > 0) rowCount / Application.PAGE_SIZE + 1 else rowCount / Application.PAGE_SIZE
  }

  def page(pageNumber: Int, filter: (String, String)*) = DB.withConnection {
    implicit connection =>
      SQL("SELECT * FROM " + table +
        (if (filter.isEmpty) "" else (for ((key, value) <- filter) yield key + " = " + "{" + key + "}").mkString(" WHERE ", " AND ", ""))
        + " ORDER BY id DESC LIMIT {passRowsCount}, {pageSize}")
        .onParams(filter.map(entry => entry._2: anorm.ParameterValue).toSeq ++
          Seq({ if (pageNumber > 0) (pageNumber - 1) * Application.PAGE_SIZE else 0 }: anorm.ParameterValue,
            Application.PAGE_SIZE: anorm.ParameterValue): _*)
        .as(parser *)
  }

}