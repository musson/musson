package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import play.Logger
import controllers.modules.bots.StatusMessage
import controllers.modules.bots.StatusProcessed
import controllers.modules.bots.StatusProcessed
import controllers.modules.bots.StatusPaused
import controllers.modules.bots.StatusUnknown
import controllers.modules.bots.StatusStopped
import controllers.modules.bots.StatusInStopProcess
import controllers.modules.bots.StatusReady

case class RobotInstance(id: Long, var ownerId: Long, var robotName: String, var status: String, var statusDescr: Option[String], var serializedId: Option[Long]) {

  def getStatus = status match {
    case "StatusProcessed"     => StatusProcessed
    case "StatusPaused"        => StatusPaused
    case "StatusStopped"       => StatusStopped
    case "StatusInStopProcess" => StatusInStopProcess
    case "StatusReady"         => StatusReady
    case _                     => StatusUnknown
  }

}

object RobotInstance extends CRUDModel[RobotInstance] {

  table = "robot_instances"

  def apply(id: Long, ownerId: Long, robotName: String, status: StatusMessage): RobotInstance =
    RobotInstance(id, ownerId, robotName, status.toString, None, None)

  parser = get[BigInteger]("id") ~
    get[BigInteger]("owner_id") ~
    get[String]("robot_name") ~
    get[String]("status") ~
    get[Option[String]]("status_descr") ~
    get[Option[BigInteger]]("serialized_id") map {
      case id ~ ownerId ~ robotName ~ status ~ statusDescr ~ serializedId =>
        RobotInstance(id.longValue,
          ownerId.longValue,
          robotName,
          status,
          statusDescr,
          serializedId.map { value => Some(value.longValue) }.getOrElse(None))
    }

  def create(robotInstance: RobotInstance): Option[RobotInstance] = DB.withConnection {
    implicit connection =>
      SQL("INSERT INTO " + table + "(id, owner_id, robot_name, status, status_descr, serialized_id) VALUES ({id}, {ownerId}, {robotName}, {status}, {statusDescr}, {seriazliedId})").on(
        'id -> robotInstance.id,
        'ownerId -> robotInstance.ownerId,
        'robotName -> robotInstance.robotName,
        'status -> robotInstance.status,
        'statusDescr -> robotInstance.statusDescr,
        'serializedId -> robotInstance.serializedId).executeInsert() match {
          case id: Some[Long] => Some(
            RobotInstance(id.get,
              robotInstance.ownerId,
              robotInstance.robotName,
              robotInstance.status,
              robotInstance.statusDescr,
              robotInstance.serializedId))
          case None => None
        }
  }

  def update(robotInstance: RobotInstance): Long = DB.withConnection {
    implicit connection =>
      SQL("UPDATE " + table + " SET owner_id = {ownerId}, robot_name = {robotName}, status = {status}, status_descr = {statusDescr}, serialized_id = {serializedId} WHERE id = {id} ").on(
        "id" -> robotInstance.id,
        "ownerId" -> robotInstance.ownerId,
        "robotName" -> robotInstance.robotName,
        "status" -> robotInstance.status,
        "statusDescr" -> robotInstance.statusDescr,
        "serializedId" -> robotInstance.serializedId).executeUpdate()
  }

}
