package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import play.Logger
import controllers.modules.bots.StatusMessage

trait ActionUserBeforeClass { val userId: Long; val accountId: Long }

trait ActionUserBefore[T <: ActionUserBeforeClass] extends CRUDModel[T] {

  def find(userId: Long, accountId: Long) = findUnique("user_id" -> userId.toString, "account_id" -> accountId.toString)

  parser = get[BigInteger]("user_id") ~
    get[BigInteger]("account_id") map { case userId ~ accountId => createInner(userId.longValue, accountId.longValue) }

  def create(actionUserBefore: T): Option[T] = DB.withConnection {
    implicit connection =>
      SQL("INSERT INTO " + table + "(user_id, account_id) VALUES ({userId}, {accountId})").on(
        'userId -> actionUserBefore.userId,
        'accountId -> actionUserBefore.accountId).executeInsert() match {
          case id: Some[Long] => Some(createInner(actionUserBefore.userId, actionUserBefore.accountId))
          case None           => None
        }
  }

  def createIfNotExists(userId: Long, accountId: Long) = find(userId, accountId).orElse(create(createInner(userId, accountId)))

  def createInner(userId: Long, accountId: Long): T

}

case class FollowedBefore(val userId: Long, val accountId: Long) extends ActionUserBeforeClass

object FollowedBefore extends ActionUserBefore[FollowedBefore] {

  table = "followed_before"

  override def createInner(userId: Long, accountId: Long): FollowedBefore = FollowedBefore(userId, accountId)

}

case class SubscriberBefore(val userId: Long, val accountId: Long) extends ActionUserBeforeClass

object SubscriberBefore extends ActionUserBefore[SubscriberBefore] {

  table = "subscriber_before"

  override def createInner(userId: Long, accountId: Long): SubscriberBefore = SubscriberBefore(userId, accountId)

}


