package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import play.Logger

case class ActivationCode(id: Long, var ownerId: Long, var action: Int, var start: Option[Long], var period: Option[Long], var code: String) {

  def isExpired: Boolean = (start, period) match {
    case (Some(startTime), Some(periodTime)) => startTime + periodTime < System.currentTimeMillis
    case _                                   => true
  }

}

object ActivationCode extends CRUDModel[ActivationCode] {

  table = "activation_codes"

  val ACTION_USER_ACCOUNT_ACTIVATION_CODE = 0

  def apply(ownerId: Long, action: Int, start: Option[Long], period: Option[Long], code: String): ActivationCode =
    ActivationCode(-1L, ownerId, action, start, period, code)

  parser = get[BigInteger]("id") ~
    get[BigInteger]("owner_id") ~
    get[Int]("action") ~
    get[Option[BigInteger]]("start") ~
    get[Option[BigInteger]]("period") ~
    get[String]("code") map {
      case id ~ ownerId ~ action ~ start ~ period ~ code =>
        ActivationCode(id.longValue,
          ownerId.longValue,
          action,
          start.map { value => Some(value.longValue) }.getOrElse(None),
          period.map { value => Some(value.longValue) }.getOrElse(None),
          code)
    }

  def create(activationCode: ActivationCode): Option[ActivationCode] = DB.withConnection {
    implicit connection =>
      SQL("INSERT INTO " + table + "(owner_id, action, start, period, code) VALUES ({ownerId}, {action}, {start}, {period}, {code})").on(
        'ownerId -> activationCode.ownerId,
        'action -> activationCode.action,
        'start -> activationCode.start,
        'period -> activationCode.period,
        'code -> activationCode.code).executeInsert() match {
          case id: Some[Long] => Some(
            ActivationCode(id.get,
              activationCode.ownerId,
              activationCode.action,
              activationCode.start,
              activationCode.period,
              activationCode.code))
          case None => None
        }
  }

  def update(activationCode: ActivationCode): Long = DB.withConnection {
    implicit connection =>
      SQL("UPDATE " + table + " SET owner_id = {ownerId}, action = {action}, start = {start}, period = {period}, code = {code} WHERE id = {id} ").on(
        "id" -> activationCode.id,
        "ownerId" -> activationCode.ownerId,
        "action" -> activationCode.action,
        "start" -> activationCode.start,
        "period" -> activationCode.period,
        "code" -> activationCode.code).executeUpdate()
  }

  def createRegisterActionCode(user: User): Option[ActivationCode] = ActivationCode.create(ActivationCode(user.id, ACTION_USER_ACCOUNT_ACTIVATION_CODE, Some(System.currentTimeMillis()), Some(86400000), System.currentTimeMillis().toString))

  def findByCode(code: String): Option[ActivationCode] = findUnique("code" -> code)

}
