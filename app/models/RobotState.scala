package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import play.Logger

case class RobotState(id: Long, instanceId: Long, data: String)

object RobotState extends CRUDModel[RobotState] {

  table = "robot_states"

  def apply(instanceId: Long, data: String): RobotState =
    RobotState(-1L, instanceId, data)

  parser = get[BigInteger]("id") ~ get[BigInteger]("instance_id") ~ get[String]("data") map { case id ~ instanceId ~ data => RobotState(id.longValue, instanceId.longValue, data) }

  def create(robotState: RobotState): Option[RobotState] = DB.withConnection {
    implicit connection =>
      SQL("INSERT INTO " + table + "(instance_id, data) VALUES (instanceId, data)").on('instanceId -> robotState.instanceId, 'data -> robotState.data).executeInsert() match {
        case id: Some[Long] => Some(RobotState(id.get, robotState.instanceId, robotState.data))
        case None           => None
      }
  }

  def update(robotState: RobotState): Long = DB.withConnection {
    implicit connection =>
      SQL("UPDATE " + table + " SET instance_id = {insatnceId}, data = {data} ").on("insatnceId" -> robotState.instanceId, "data" -> robotState.data).executeUpdate()
  }

}
