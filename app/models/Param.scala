package models

import java.math.BigInteger
import anorm.~
import anorm.NamedParameter.string
import anorm.NamedParameter.symbol
import anorm.SQL
import anorm.SqlParser.get
import anorm.sqlToSimple
import play.api.Play.current
import play.api.db.DB
import play.Logger

case class Param(id: Long, name: String, var value: String) {
  def int = value.toInt
  def long = value.toLong
}

object Param extends CRUDModel[Param] {

  table = "params"

  val PARAM_LAST_POSTS_LIMIT = "last_posts_limit"

  val PARAM_TIMEZONE = "timezone"

  val PARAM_FILES_FOLDER = "files_folder"

  val PARAM_PROJECT_NAME = "project_name"

  val PARAM_PROJECT_DESCRIPTION = "project_description"
  
  val PARAM_LOGIN_PAGE_VISIBLE = "login_page_visible"
  
  val PARAM_REGISTER_IS_ACTIVE = "register_is_active"

  val PARAM_THEME_NAME = "theme_name"

  val PARAM_VALUE_DEFAULT_TIMEZONE = "UTC"

  val PARAM_VALUE_DEFAULT_PROJECT_NAME = "Musson"

  val PARAM_VALUE_DEFAULT_THEME_NAME = "keno_landing"

  val PARAM_VALUE_DEFAULT_PROJECT_DESCRIPTION = "play based cms"

  val PARAM_VALUE_DEFAULT_FILES_FOLDER = "files"

  val PARAM_VALUE_DEFAULT_LAST_POSTS_LIMIT = 10
  
  val PARAM_VALUE_DEFAULT_LOGIN_PAGE_VISIBLE = true
  
  val PARAM_VALUE_DEFAULT_REGISTER_IS_ACTIVE = false


  def apply(name: String, value: String): Param = Param(-1L, name, value)

  parser = get[BigInteger]("id") ~
    get[String]("name") ~
    get[String]("value") map {
      case id ~ name ~ value => Param(id.longValue, name, value)
    }

  def create(param: Param): Option[Param] = DB.withConnection {
    implicit connection =>
      SQL("insert into " + table + "(name, value) values ({name}, {value})").on(
        'name -> param.name,
        'value -> param.value).executeInsert() match {
          case id: Some[Long] => Some(Param(id.get, param.name, param.value))
          case None           => None
        }
  }

  /**
   *
   * TODO: Needs caching
   *
   */
  def findByName(name: String): Option[Param] = DB.withConnection {
    implicit connection =>
      SQL("select * from " + table + " where name = {name}").on("name" -> name).as(parser.singleOpt)
  }

  def update(param: Param): Long = DB.withConnection {
    implicit connection =>
      SQL("""
            UPDATE params 
            SET name = {name}, value = {value}      
            WHERE id = {id}
        """).on(
        "id" -> param.id,
        "name" -> param.name,
        "value" -> param.value).executeUpdate()
  }

  def update(name: String, value: String): Boolean = Param.findByName(name).map { param =>
    if (param.value != value) {
      param.value = value
      if (update(param) != 1L) Logger.error("Can't update parameter with name: " + name + " with value \"" + value + "\"")
    }
    true
  }.getOrElse {
    Logger.error("Can't find parameter with name: " + name)
    false
  }

}
