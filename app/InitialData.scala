import models.Content
import models.Param
import models.Permission
import models.User
import play.api.Logger
import controllers.Datetime
import controllers.modules.instagram.InstagramModule
import controllers.modules.instagram.InstagramParamsInitializer

object InitialData {
  def insert = {
    if (Param.count == 0) {
      Logger.debug("Upload initial data for params")
      Seq(
        Param(Param.PARAM_TIMEZONE, Param.PARAM_VALUE_DEFAULT_TIMEZONE),
        Param(Param.PARAM_PROJECT_NAME, Param.PARAM_VALUE_DEFAULT_PROJECT_NAME),
        Param(Param.PARAM_PROJECT_DESCRIPTION, Param.PARAM_VALUE_DEFAULT_PROJECT_DESCRIPTION),
        Param(Param.PARAM_THEME_NAME, Param.PARAM_VALUE_DEFAULT_THEME_NAME),
        Param(Param.PARAM_FILES_FOLDER, Param.PARAM_VALUE_DEFAULT_FILES_FOLDER),
        Param(Param.PARAM_LAST_POSTS_LIMIT, Param.PARAM_VALUE_DEFAULT_LAST_POSTS_LIMIT.toString),
        Param(Param.PARAM_LOGIN_PAGE_VISIBLE, Param.PARAM_VALUE_DEFAULT_LOGIN_PAGE_VISIBLE.toString),
        Param(Param.PARAM_REGISTER_IS_ACTIVE, Param.PARAM_VALUE_DEFAULT_REGISTER_IS_ACTIVE.toString)).foreach(Param.create)
      InstagramParamsInitializer.init
    }
    if (Permission.count == 0) {
      Logger.debug("Upload initial data for permissions")
      Seq(
        Permission(Permission.PERMISSION_ADMINISTRATOR),
        Permission(Permission.PERMISSION_NORMAL_USER),
        Permission(Permission.PERMISSION_GUEST_USER)).foreach(Permission.create)
    }
    if (User.count == 0) {
      Logger.debug("Upload initial data for users")
      Seq(
        User(
          "admin@sample.sam",
          Some("$2a$10$PXRPcovCre8j1e/n3tLyouCIQZWQwQ0T8zcOof4Yidnge/W1dKv3q"), // "admin2015"
          Permission.administartor,
          Some(1),
          User.USER_STATUS_OFFLINE,
          User.ACCOUNT_STATUS_ENABLED,
          Some("Sample"),
          Some("Admin"),
          "Europe/Moscow",
          Some("+7 777 777 77 77 77 78"), User.USER_TYPE_NORMAL),
        User(
          "user@gsample.sam",
          Some("$2a$10$ZMiUJ/EO5W4fgN3uZbvsEuBfkXuYMqO/TDVRevcSpgBBkuwbH8UVK"), // "user2015"
          Permission.normalUser,
          Some(1),
          User.USER_STATUS_OFFLINE,
          User.ACCOUNT_STATUS_ENABLED,
          Some("Sample"),
          Some("User"),
          "Europe/Kiev",
          Some("+7 777 777 77 77 77 77"), User.USER_TYPE_NORMAL)).foreach(User.create)
    }
    if (Content.count == 0) {
      Logger.debug("Upload initial data for content")
      Seq(
        Content(1, Some("Гость Вендетта"), Some("2014/08/guest_vendetta.jpg"), None, Content.KIND_ATTACHMENT, Content.ROLE_NO, Some("jpg")),
        Content(1, Some("Главная"), Some("Проект по автоматизации сбора статистии и ислледования Instagram аккаунтов."), Some(Content.STATE_PUBLISHED), Content.KIND_PAGE, Content.ROLE_INDEX, None)).foreach(Content.create)
    }
  }
}
